<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 18.06.2014
 * File: About_Content_Link.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $this WebStatic\Core\Content */

$this->DOMNode->parentNode->removeChild( $this->DOMNode );
$this->DOMNode = $this->getDOMNode()->ownerDocument->createElement( 'div' );