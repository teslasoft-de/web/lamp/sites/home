<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 18.06.2014
 * File: About_Content_Link.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $this WebStatic\Core\Content */

// Find page by column title and get the link to it.

// Cancel if not a link target
$menuItem = $this->getPage()->getMenu()->getSite()->getMenu()->FindItem( $this->Value );

if (!$menuItem)
    return;

$a = $this->DOMNode;

$href = $a->attributes->getNamedItem( 'href' );
if(!$href)
    $href = $a->appendChild( $a->ownerDocument->createAttribute( 'href' ) );
$href->nodeValue = $menuItem->getQueryPath();

$title = $a->attributes->getNamedItem( 'title' );
if(!$title)
    $title = $a->appendChild( $a->ownerDocument->createAttribute( 'title' ) );

AppStatic\Data\XmlUtility::SetHtmlContent( $title, $menuItem->getTitle() );
$this->Value = $menuItem->getTitle();