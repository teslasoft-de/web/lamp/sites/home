<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 18.06.2014
 * File: About_Content_Link.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

use AppStatic\Collections\ArrayObjectPropertyBase;
use AppStatic\Data\XmlUtility;

/* @var $this WebStatic\Core\Content */

function generateSitemap( ArrayObjectPropertyBase $menuItems, &$sitemap )
{
    /* @var $menuItem WebStatic\MenuItem */
    foreach ($menuItems as $menuItem) {
        if(!$menuItem->getQueryPath() && count($menuItem) == 0)
            continue;
        $sitemap .= "<li>";
        if($menuItem->getIcon())
            $sitemap .= "<span class=\"{$menuItem->getIcon()}\"></span>";
        
        $sitemap .= $menuItem->getQueryPath()
            ? "<a class=\"sitemap-link\" href='{$menuItem->getQueryPath()}'>{$menuItem->getTitle()}</a>"
            : $menuItem->getName();
        
        if (count( $menuItem ) > 0) {
            $sitemap .= "<ul>";
            generateSitemap( $menuItem, $sitemap );
            $sitemap .= "</ul>";
        }
        $sitemap .= "</li>";
    }
}
generateSitemap( $this->getPage()->getMenu()->getSite()->getMenu(), $sitemap );

$this->DOMNode->parentNode->removeChild( $this->DOMNode );
$this->DOMNode = $this->getDOMNode()->ownerDocument->createElement( 'ul' );
XmlUtility::SetAttribute( $this->DOMNode, 'id', 'sitemap' );
XmlUtility::SetHtmlContent( $this->DOMNode, $sitemap );

$this->NodeHandled =  false;