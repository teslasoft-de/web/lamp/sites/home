<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 07.06.2014
 * File: Home_Column_Description.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 * */

use AppStatic\Core\String;
use AppStatic\Data\XmlUtility;

/* @var $this WebStatic\Core\Content */

// Find page by column title and get the description.

foreach ($this->Parent as $key => $value)
    if (String::EndsWidth( $key, '- Title' ))
        break;

/* @var $menuItem WebStatic\MenuItem */
$menuItem = $this->Page->getMenu()->getSite()->getMenu()->FindItem( $value->getValue() );

if (!$menuItem)
    return;

XmlUtility::SetHtmlContent( $this->DOMNode, $menuItem->getPage( false )->getDescription() );

$this->ScriptHandled = true;