<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 04.07.2014
 * File: Salutation_Select.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

use AppStatic\Data\XmlUtility;
/* @var $this WebStatic\Core\Content */

XmlUtility::SetAttribute( $this->getDOMNode(), 'data-style', isset( $_SESSION[ 'inputSalutationError' ] ) ? 'btn-danger' : 'btn-default' );