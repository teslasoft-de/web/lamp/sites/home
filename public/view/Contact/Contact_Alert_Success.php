<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 03.07.2014
 * File: Contact_Alert_Success.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 * */

use AppStatic\Web\Mail\EmailAddress;
use AppStatic\Web\UI\TemplateBase;

/* @var $this WebStatic\Core\Content */

session_start();
$submit = filter_input( INPUT_POST, 'contactSubmit' );
$hasErrors = false;

function resetForm()
{
    session_destroy();
    session_start();
    $_SESSION[ 'inputHumanX' ] = $spamCheckX = rand( 1, 9 );
    $_SESSION[ 'inputHumanY' ] = $spamCheckY = rand( 1, 9 );
    $_SESSION[ 'inputHumanValue' ] = $spamCheckX + $spamCheckY;
    
    $_SESSION[ 'inputSalutation' ] = 0;
    $_SESSION[ 'inputName' ] = null;
    $_SESSION[ 'inputTel' ] = null;
    $_SESSION[ 'inputCompany' ] = null;
    $_SESSION[ 'inputWebsiteURL' ] = null;
    $_SESSION[ 'inputEmail' ] = null;
    $_SESSION[ 'inputSubject' ] = null;
    $_SESSION[ 'inputMessage' ] = null;
    $_SESSION[ 'inputHuman' ] = null;
}

if (!$submit) {
    resetForm();
} else {
    $salutation = filter_input( INPUT_POST, 'inputSalutation', FILTER_VALIDATE_INT );
    if (!$salutation || $salutation < 1 || $salutation > 2)
        $_SESSION[ 'inputSalutationError' ] = $hasErrors = true;
    else
        unset( $_SESSION[ 'inputSalutationError' ] );
    $_SESSION[ 'inputSalutation' ] = $salutation;

    // Validate Name
    $name = preg_replace( "~\s+~", ' ', trim( filter_input( INPUT_POST, 'inputName', FILTER_SANITIZE_STRING ) ) );
    if (!$name || TemplateBase::IsBadFormValue( $name ) || strlen( $name ) > 70)
        $_SESSION[ 'inputNameError' ] = $hasErrors = true;
    else
        unset( $_SESSION[ 'inputNameError' ] );
    $_SESSION[ 'inputName' ] = substr( $name, 0, 70 );
    
    // Validate Telephone
    $tel = preg_replace( "~\s+~", ' ', trim( filter_input( INPUT_POST, 'inputTel', FILTER_SANITIZE_STRING ) ) );
    if ($tel && (strlen( $tel ) > 20 || !is_numeric(preg_replace( "~[\+\(\)\-/\s]~", '', $tel ))))
        $_SESSION[ 'inputTelError' ] = $hasErrors = true;
    else
        unset( $_SESSION[ 'inputTelError' ] );
    $_SESSION[ 'inputTel' ] = substr( $tel, 0, 20 );
    
    // Validate Company
    $company = preg_replace( "~\s+~", ' ', trim( filter_input( INPUT_POST, 'inputCompany', FILTER_SANITIZE_STRING ) ) );
    if ($company && strlen( $company ) > 70)
        $_SESSION[ 'inputCompanyError' ] = $hasErrors = true;
    else
        unset( $_SESSION[ 'inputCompanyError' ] );
    $_SESSION[ 'inputCompany' ] = substr( $company, 0, 70 );

    // Validate URL
    $url = filter_var( trim( filter_input( INPUT_POST, 'inputWebsiteURL', FILTER_SANITIZE_URL ) ), FILTER_VALIDATE_URL );
    if ($url) {
        if (strlen( $url ) > 2000)
            $_SESSION[ 'inputWebsiteURLError' ] = $hasErrors = true;
        else {
            switch (\AppStatic\Web\HttpUtility::GetHttpResponseCode( $url, filter_input( INPUT_SERVER, 'SERVER_NAME' ) )) {
                case 200:
                case 201:
                case 202:
                case 301:
                case 302:
                    unset( $_SESSION[ 'inputWebsiteURLError' ] );
                    break;
                default:
                    $_SESSION[ 'inputWebsiteURLError' ] = $hasErrors = true;
                    break;
            }
        }
    } else
        unset( $_SESSION[ 'inputWebsiteURLError' ] );
    $_SESSION[ 'inputWebsiteURL' ] = substr( $url, 0, 2000 );

    // Validate Subject
    $subject = preg_replace( "~\s+~", ' ', trim( filter_input( INPUT_POST, 'inputSubject', FILTER_SANITIZE_STRING ) ) );
    if (!$subject || TemplateBase::IsBadFormValue( $subject ) || strlen( $subject ) > 255)
        $_SESSION[ 'inputSubjectError' ] = $hasErrors = true;
    else
        unset( $_SESSION[ 'inputSubjectError' ] );
    $_SESSION[ 'inputSubject' ] = substr( $subject, 0, 255 );

    // Validate Message
    $message = preg_replace( "~\s+~", ' ', trim( filter_input( INPUT_POST, 'inputMessage', FILTER_SANITIZE_STRING ) ) );
    if (!$message || TemplateBase::IsBadFormValue( $message ) || strlen( $message ) > 4096)
        $_SESSION[ 'inputMessageError' ] = $hasErrors = true;
    else
        unset( $_SESSION[ 'inputMessageError' ] );
    $_SESSION[ 'inputMessage' ] = substr( $message, 0, 4096 );

    // Validate human
    $spamCheck = filter_var( filter_input( INPUT_POST, 'inputHuman', FILTER_SANITIZE_NUMBER_INT ), FILTER_VALIDATE_INT );
    if (!$spamCheck || $spamCheck != $_SESSION[ 'inputHumanValue' ]) {
        $_SESSION[ 'inputHumanError' ] = $hasErrors = true;
        $_SESSION[ 'inputHuman' ] = null;
    } else {
        unset( $_SESSION[ 'inputHumanError' ] );
        $_SESSION[ 'inputHuman' ] = $spamCheck;
    }

    // Validate Email
    $email = filter_var( trim( filter_input( INPUT_POST, 'inputEmail', FILTER_SANITIZE_EMAIL ) ), FILTER_VALIDATE_EMAIL );
    if (!$email || strlen( $email ) > 253) {
        $_SESSION[ 'inputEmailError' ] = $hasErrors = true;
    } else if (!$hasErrors) {
        $address = new EmailAddress( $email );
        if (!$address->IsValid())
            $hasErrors = true;
        else if ($address->IsBlacklisted()) {
            $hasErrors = true;
            $_SESSION[ 'inputEmailError' ] = "Email is blacklisted";
        } else if (!$address->getValidator()->ValidateEmail( true, 'info@teslasoft.de', 'www.teslasoft.de' )) {
            $hasErrors = true;

            if ($address->getValidator()->getConnectionRefused())
                $_SESSION[ 'inputEmailError' ] = "Your e-mail server '{$address->getValidator()->getDomain()}' refused connection.";
            else if (preg_match( '/(None of the mailservers listed.+)|(.*DNS.*)/', $address->getValidator()->getError() ) == 1) // Mailservers not available
                $_SESSION[ 'inputEmailError' ] = "Your e-mail server '{$address->getValidator()->getDomain()}' is not reachable.";
            else {
                $matches = array();
                $_SESSION[ 'inputEmailError' ] = "Your e-mail address is invalid. ";
                $_SESSION[ 'inputEmailError' ] .= preg_match( '/Unverified address:.+said:\s*(.+)/', $address->getValidator()->getError(), $matches ) == 1 ? "E-Mail Server Response:" . $matches[ 1 ] : $address->getValidator()->getError();
            }
        }
    }
    $_SESSION[ 'inputEmail' ] = substr( $email, 0, 253 );
}

if($submit && !$hasErrors){
    \AppStatic\Autoload\Using::Directory( APPSTATIC_INC_RMAIL );
    $mail = new \Rmail();
    $charset = 'UTF-8';
    $mail->setHeadCharset( $charset );
    $mail->setTextCharset( $charset );
    $mail->setHTMLCharset( $charset );
    $from = mb_encode_mimeheader( 'TESLÅSOFT', $charset, 'Q' ) . ' <info@teslasoft.de>';
    $sender = mb_encode_mimeheader( $name, $charset, 'Q' ) . " <$email>";
    
    $mail->setFrom( $sender );
    //$mail->setCc( $sender );
    $mail->setReturnPath( 'info@teslasoft.de' );
    $mail->setSubject( $subject );

    $lang = $this->getPage()->getMenu()->getSite()->getUserDefinedLanguage()
            ? $this->getPage()->getMenu()->getSite()->getUserDefinedLanguage()
            : $this->getPage()->getMenu()->getSite()->getUserLanguage();
    $salutations = array(
        'de' => array( 1 => 'Frau', 2 => 'Herr' ),
        'en' => array( 1 => 'Mrs', 2 => 'Mr' ) );
    
    $email = array("{$salutations[$lang][$salutation]} $name hat eine Nachricht über das Kontaktformular hinterlassen:" . PHP_EOL);
    
    $email []= $message . PHP_EOL;
    if($tel)
        $email []= "Telefon: $tel";
    
    if($company)
        $email []= "Firma: $company";
    
    if($url)
        $email []= "Website: $url";
    
    $email []= PHP_EOL . "Browser: " .  filter_input( INPUT_SERVER, 'HTTP_USER_AGENT' );
    $email []= "Browser-Sprache: " .  filter_input( INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE' );
    $email []= "IP: " .  filter_input( INPUT_SERVER, 'REMOTE_ADDR' );
    
    $email []= <<<EOT
___________________________
TESLÅSOFT

EOT;
    $email []=
        (filter_input( INPUT_SERVER, 'SERVER_PORT' ) == 443 ? 'https://' : 'http://')
        . filter_input( INPUT_SERVER, 'HTTP_HOST' )
        . urldecode( parse_url( filter_input( INPUT_SERVER, 'REQUEST_URI' ), PHP_URL_PATH ) );
    $email []= gethostname();

    $mail->setText( implode( PHP_EOL, $email ) );
    $mail->send( array( $from ) );
    resetForm();
}

$this->ScriptHandled = $this->NodeHandled = $hasErrors || !$submit;
// Display the error message only on submit with input errors
$_SESSION[ 'contactError' ] = $submit && $hasErrors;