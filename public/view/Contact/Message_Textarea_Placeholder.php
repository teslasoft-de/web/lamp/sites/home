<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 12.07.2014
 * File: Message_Textarea_Placeholder.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

use AppStatic\Data\XmlUtility;
/* @var $this WebStatic\Core\Content */

$array = (array) $this->Parent;
XmlUtility::SetAttribute( current($array)->DOMNode, 'placeholder', $this->Value );
$this->NodeHandled = true;