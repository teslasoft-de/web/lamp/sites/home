<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 18.06.2014
 * File: Contact_Company_Email.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

use AppStatic\Data\XmlUtility;
/* @var $this WebStatic\Core\Content */

$email = preg_split('~@~', $this->getValue() );
XmlUtility::SetAttribute( $this->getDOMNode(), 'prepend', $email[0] );
XmlUtility::SetAttribute( $this->getDOMNode(), 'append', $email[1] );
$this->ScriptHandled = true;