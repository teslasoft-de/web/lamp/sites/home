<?php
/**
 * Created by PhpStorm.
 * User: Cosmo
 * Date: 25.08.2016
 * Time: 00:40
 */

$language_error = [
    'title' => 'Interner Server Fehler.',
    'sub-title' => 'Ihre Anfrage konnte nicht bearbeitet werden.',
    'alert' => 'Es ist ein Fehler aufgetreten. Bitte versuchen Sie es später noch einmal.',
    400 => ['400 Bad Request', 'The request cannot be fulfilled due to bad syntax.'],
    403 => ['403 Forbidden', 'The server has refused to fulfil your request.'],
    404 => ['404 Nicht gefunden', 'Die angeforderte Seite konnte nicht gefunden werden.'],
    405 => ['405 Method Not Allowed', 'The method specified in the request is not allowed for the specified resource.'],
    408 => ['408 Request Timeout', 'Your browser failed to send a request in the time allowed by the server.'],
    500 => ['500 Interner Server Fehler', 'Die Anfrage konnte aufgrund einer unerwarteten Bedingung nicht abgeschlossen werden.'],
    502 => ['502 Bad Gateway', 'The server received an invalid response while trying to carry out the request.'],
    504 => ['504 Gateway Timeout', 'The upstream server failed to send a request in the time allowed by the server.']
];