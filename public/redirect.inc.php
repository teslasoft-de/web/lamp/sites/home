<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 29.07.2014
 * File: redirect.inc.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

$permanent = array(
    '/de/it-dienstleistungen/produktentwicklung-endanwender-software' => '/de/endanwender-software',
    '/it-dienstleistungen/produktentwicklung-endanwender-software' => '/endanwender-software',
    '/it-dienstleistungen/endanwender-software' => '/endanwender-software',
    '/de/it-dienstleistungen/endanwender-software' => '/de/endanwender-software',
    '/en/it-services/consumer-software-product-development' => '/en/consumer-software',
    '/en/it-services/consumer-software' => '/en/consumer-software',
    '/en/it-services/e-mail-marketing-performance-metrics' => '/en/performance-marketing',
    '/en/about/legal-notice' => '/en/legal-notice',
    '/en/about/privacy-policy' => '/en/privacy-policy',
    '/datenschutzerklärung' => '/datenschutzerklaerung'
);