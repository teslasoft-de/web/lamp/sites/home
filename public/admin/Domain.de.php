<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 13.01.2014
 * File: Domain.de.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

define( 'SITE_LANGUAGE', 'de' );
require_once __DIR__ .  '/Domain.inc.php';

// Create site menu
CreateMenu( $site, $menuItem, SITE_LANGUAGE,
    '/',
    'Home', 'Startseite', 'glyphicon glyphicon-home fa-fw',
    $homeTemplate,
    'Home', 'Teslasoft - We make IT life easier.' );

CreateMenu( $site, $menuItem, SITE_LANGUAGE,
    '/endanwender-software',
    'Consumer Software', 'Endanwender-Software', 'fa fa-cubes fa-fw',
    $pageTemplate,
    'Consumer Software', 'Endanwender-Software - Teslasoft' );

$servicesMenu = CreateMenu( $site, $subMenu, SITE_LANGUAGE, null,
    'IT Services', 'IT-Dienstleistungen', 'fa el el-th fa-fw' );

CreateSubMenu( $servicesMenu, $subMenuItem, SITE_LANGUAGE,
    '/it-dienstleistungen/performance-marketing',
    'Performance Marketing', 'Performance-Marketing', 'fa fa-bar-chart-o fa-fw',
    $pageTemplate,
    'Performance Marketing', 'Performance-Marketing - Teslasoft' );

CreateSubMenu( $servicesMenu, $subMenuItem, SITE_LANGUAGE,
    '/it-dienstleistungen/it-consulting',
    'IT-Consulting', 'IT-Consulting', 'fa fa-users fa-fw',
    $pageTemplate,
    'IT-Consulting', 'IT-Consulting - Teslasoft' );

CreateSubMenu( $servicesMenu, $subMenuItem, SITE_LANGUAGE,
    '/it-dienstleistungen/web-hosting',
    'Web-Hosting', 'Web-Hosting', 'fa fa-cogs fa-fw',
    $pageTemplate,
    'Web-Hosting', 'Web-Hosting - Teslasoft' );

CreateSubMenu( $servicesMenu, $subMenuItem, SITE_LANGUAGE,
    '/it-dienstleistungen/datenbank-abfragesysteme',
    'Data Query Systems', 'Datenbank-Abfragesysteme', 'fa fa-database fa-fw',
    $pageTemplate,
    'Data Query Systems', 'Datenbank-Abfragesysteme - Teslasoft' );

CreateSubMenu( $servicesMenu, $subMenuItem, SITE_LANGUAGE,
    '/it-dienstleistungen/it-hardware-support-networking',
    'IT Hardware Support Networking', 'Demnächst: IT-Hardware-Support-Networking', 'fa el el-network fa-fw',
    $pageTemplate,
    'IT Hardware Support Networking', 'Demnächst: IT-Hardware-Support-Networking - Teslasoft' );

CreateMenu( $site, $menuItem, SITE_LANGUAGE,
    '/kontakt',
    'Contact', 'Kontakt', 'glyphicon glyphicon-envelope fa-fw',
    $contactTemplate,
    'Contact', 'Kontaktieren Sie uns - Teslasoft' );

$aboutMenu = CreateMenu( $site, $subMenu, SITE_LANGUAGE, null,
    'About', 'Info', 'glyphicon glyphicon-info-sign fa-fw' );

CreateSubMenu( $aboutMenu, $subMenuItem, SITE_LANGUAGE,
    '/impressum',
    'Legal Notice', 'Impressum', 'fa fa-gavel fa-fw',
    $pageTemplate,
    'Legal Notice', 'Impressum - Teslasoft' );

CreateSubMenu( $aboutMenu, $subMenuItem, SITE_LANGUAGE,
    '/datenschutzerklaerung',
    'Privacy Policy', 'Datenschutzerklärung', 'glyphicon glyphicon-eye-open fa-fw',
    $pageTemplate,
    'Privacy Policy', 'Datenschutzerklärung von Teslasoft' );

CreateSubMenu( $aboutMenu, $subMenuDivider, SITE_LANGUAGE, null,
    'Help_Divider' );

CreateSubMenu( $aboutMenu, $subMenuHeader, SITE_LANGUAGE, null,
    'Help', 'Hilfe', 'fa fa-life-ring fa-fw' );

CreateSubMenu( $aboutMenu, $subMenuItem, SITE_LANGUAGE,
    '/sitemap',
    'Sitemap', 'Sitemap', 'fa fa-sitemap fa-fw',
    $pageTemplate,
    'Sitemap', 'Teslasoft - Sitemap' );

CreateSubMenu( $aboutMenu, $subMenuItem, SITE_LANGUAGE,
    '#browser-support',
    'Browser Support', 'Unterstützte Browser', 'fa fa-photo fa-fw',
    $wbsTemplate,
    'Browser Support', 'Unterstützte Browser' );

CreateSubMenu( $aboutMenu, $subMenuDivider, SITE_LANGUAGE, null,
    'About_Divider' );

CreateSubMenu( $aboutMenu, $subMenuItem, SITE_LANGUAGE,
    '#about',
    'About...', 'Über...', 'fa fa-info fa-fw',
    $aboutTemplate,
    'About...', 'Über' );

CreateMenu($site, $langSelect, SITE_LANGUAGE, null,
    'Language Select' );