<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 13.01.2014
 * File: Domain.inc.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

require_once __DIR__ .  '/Include.inc.php';
use AppStatic\Configuration\System;
use AppStatic\Web\DomainName;
use WebStatic\Core\Template;

Template::$TemplateMode = true;
// Create template items
require_once __DIR__ . '/../template/site.php';
require_once __DIR__ . '/../template/Home/home.php';
require_once __DIR__ . '/../template/page.php';
require_once __DIR__ . '/../template/Contact/contact.php';
require_once __DIR__ . '/../template/About/about.php';
require_once __DIR__ . '/../template/About/Browser Support/browser-support.php';

$domainName = !System::IsLocal() ? DomainName::getCurrent() : new DomainName( 'dev.teslasoft.de' );
// Create the Domain
$domain = CreateDomain( $domainName );

// Create site for the domain
$site = $domain->SetSite( $domainName->GetLabel(3), $siteTemplate, 'Teslasoft Company', '/srv/www/teslasoft/public', SITE_LANGUAGE );

$menuItem       = $siteTemplate->MenuItem;
$subMenu        = $siteTemplate->SubMenu;
$subMenuItem    = $siteTemplate->SubMenuItem;
$subMenuDivider = $siteTemplate->SubMenuItem_Divider;
$subMenuHeader  = $siteTemplate->SubMenuItem_Header;
$langSelect     = $siteTemplate->LanguageSelect;