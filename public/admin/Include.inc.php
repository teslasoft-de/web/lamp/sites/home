<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 01.06.2014
 * File: Include.inc.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

use AppStatic\Web\DomainName;
use WebStatic\Core\Domain;
use WebStatic\Core\MenuItem;
use WebStatic\Core\Site;
use WebStatic\Core\Template;
use WebStatic\Core\TemplateItem;

require_once '../WebStatic/config/config.inc.php';
require_once '../template/include.inc.php';

\WebStatic\Configuration\mysqli_connect_www_siteadmin();

function CreateDomain( DomainName $domainName )
{
    $domain = new Domain( $domainName );
    $domain->Load();
    if(!$domain->getID())
        $domain->Save();
    return $domain;
}

/**
 * @param Site $site
 * @param TemplateItem $menuTemplateItem
 * @param null $language
 * @param null $queryPath
 * @param null $name
 * @param null $title
 * @param null $icon
 * @param Template $pageTemplate
 * @param null $pageName
 * @param null $siteTitle
 * @param bool $allowRobots
 *
 * @return MenuItem
 */
function CreateMenu(
    Site $site, TemplateItem $menuTemplateItem, $language = null, $queryPath = null, $name = null, $title = null, $icon = null,
    Template $pageTemplate = null, $pageName = null, $siteTitle = null, $allowRobots = true )
{
    // Create the menu item
    $menuItem = $site->getMenu()->SetItem( $menuTemplateItem, $language, $queryPath, $name, $title, $icon );
    if(!$pageTemplate)
        return $menuItem;

    $page = $menuItem->SetPage( $pageTemplate, $language, $pageName, $siteTitle, $allowRobots );
    $contentScriptPath = \WebStatic\CONTENT_PATH . "$pageName.$language.php";
    if(is_file( $contentScriptPath )){
        /** @noinspection PhpIncludeInspection */
        require_once $contentScriptPath;
        return null;
    }

    $contentScriptPath = \WebStatic\CONTENT_PATH . "$pageName.php";
    if(is_file( $contentScriptPath ))
        /** @noinspection PhpIncludeInspection */
        require_once $contentScriptPath;
}

function CreateSubMenu(
    MenuItem $menuItem, $menuTemplateItem, $language, $queryPath, $name, $title = null, $icon = null,
    Template $pageTemplate = null, $siteName = null, $siteTitle = null, $allowRobots = true )
{
    // Add the menu item
    $menuItem = $menuItem->SetChild( $menuTemplateItem, $language, $queryPath, $name, $title, $icon );

    if(!$pageTemplate)
        return;
    // Create the page
    $page = $menuItem->SetPage( $pageTemplate, $language, $siteName, $siteTitle, $allowRobots );
    $contentScriptPath = \WebStatic\CONTENT_PATH . "{$menuItem->getParent()->getName()}/$siteName.$language.php";
    if(is_file( $contentScriptPath )){
        /** @noinspection PhpIncludeInspection */
        require_once $contentScriptPath;
        return null;
    }

    $contentScriptPath = \WebStatic\CONTENT_PATH . "{$menuItem->getParent()->getName()}/$siteName.php";
    if(is_file( $contentScriptPath ))
        /** @noinspection PhpIncludeInspection */
        require_once $contentScriptPath;
}