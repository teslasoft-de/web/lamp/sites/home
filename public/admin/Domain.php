<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 13.01.2014
 * File: Domain.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

define( 'SITE_LANGUAGE', 'en' );
require_once __DIR__ .  '/Domain.inc.php';

// Create site menu
CreateMenu( $site, $menuItem, SITE_LANGUAGE,
    '/',
    'Home', 'Home', 'glyphicon glyphicon-home fa-fw',
    $homeTemplate,
    'Home', 'Teslasoft - We make IT life easier.' );

CreateMenu( $site, $menuItem, SITE_LANGUAGE,
    '/consumer-software',
    'Consumer Software', 'Consumer Software', 'fa fa-cubes fa-fw',
    $pageTemplate,
    'Consumer Software', 'Consumer Software - Teslasoft' );

$servicesMenu = CreateMenu( $site, $subMenu, SITE_LANGUAGE, null,
    'IT Services', 'IT Services', 'fa el el-th fa-fw' );

CreateSubMenu( $servicesMenu, $subMenuItem, SITE_LANGUAGE,
    '/it-services/performance-marketing',
    'Performance Marketing', 'Performance Marketing', 'fa fa-bar-chart-o fa-fw',
    $pageTemplate,
    'Performance Marketing', 'Performance Marketing - Teslasoft' );

CreateSubMenu( $servicesMenu, $subMenuItem, SITE_LANGUAGE,
    '/it-services/it-consulting',
    'IT-Consulting', 'IT-Consulting', 'fa fa-users fa-fw',
    $pageTemplate,
    'IT-Consulting', 'IT-Consulting - Teslasoft' );

CreateSubMenu( $servicesMenu, $subMenuItem, SITE_LANGUAGE,
    '/it-services/web-hosting',
    'Web-Hosting', 'Web-Hosting', 'fa fa-cogs fa-fw',
    $pageTemplate,
    'Web-Hosting', 'Web-Hosting - Teslasoft' );

CreateSubMenu( $servicesMenu, $subMenuItem, SITE_LANGUAGE,
    '/it-services/data-query-systems',
    'Data Query Systems', 'Data Query Systems', 'fa fa-database fa-fw',
    $pageTemplate,
    'Data Query Systems', 'Data Query Systems - Teslasoft' );

CreateSubMenu( $servicesMenu, $subMenuItem, SITE_LANGUAGE,
    '/it-services/it-hardware-support-networking',
    'IT Hardware Support Networking', 'Coming Soon: IT Hardware Support Networking', 'fa el el-network fa-fw',
    $pageTemplate,
    'IT Hardware Support Networking', 'Coming Soon: IT Hardware Support Networking - Teslasoft' );

CreateMenu( $site, $menuItem, SITE_LANGUAGE,
    '/contact',
    'Contact', 'Contact', 'glyphicon glyphicon-envelope fa-fw',
    $contactTemplate,
    'Contact', 'Contact Us - Teslasoft' );

$aboutMenu = CreateMenu( $site, $subMenu, SITE_LANGUAGE, null,
    'About', 'About', 'glyphicon glyphicon-info-sign fa-fw' );

CreateSubMenu( $aboutMenu, $subMenuItem, SITE_LANGUAGE,
    '/legal-notice',
    'Legal Notice', 'Legal Notice', 'fa fa-gavel fa-fw',
    $pageTemplate,
    'Legal Notice', 'Teslasoft - Legal Notice' );

CreateSubMenu( $aboutMenu, $subMenuItem, SITE_LANGUAGE,
    '/privacy-policy',
    'Privacy Policy', 'Privacy Policy', 'glyphicon glyphicon-eye-open fa-fw',
    $pageTemplate,
    'Privacy Policy', 'Teslasoft - Privacy Policy' );

CreateSubMenu( $aboutMenu, $subMenuDivider, SITE_LANGUAGE, null,
    'Help_Divider' );

CreateSubMenu( $aboutMenu, $subMenuHeader, SITE_LANGUAGE, null,
    'Help', 'Help', 'fa fa-life-ring fa-fw' );

CreateSubMenu( $aboutMenu, $subMenuItem, SITE_LANGUAGE,
    '/sitemap',
    'Sitemap', 'Sitemap', 'fa fa-sitemap fa-fw',
    $pageTemplate,
    'Sitemap', 'Teslasoft - Sitemap' );

CreateSubMenu( $aboutMenu, $subMenuItem, SITE_LANGUAGE,
    '#browser-support',
    'Browser Support', 'Browser Support', 'fa fa-photo fa-fw',
    $wbsTemplate,
    'Browser Support', 'Browser Support' );

CreateSubMenu( $aboutMenu, $subMenuDivider, SITE_LANGUAGE, null,
    'About_Divider' );

CreateSubMenu( $aboutMenu, $subMenuItem, SITE_LANGUAGE,
    '#about',
    'About...', 'About...', 'fa fa-info fa-fw',
    $aboutTemplate,
    'About...', 'About' );

CreateMenu( $site, $langSelect, SITE_LANGUAGE, null,
    'Language Select' );