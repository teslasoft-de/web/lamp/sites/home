<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 22.05.2014
 * File: home.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

use AppStatic\Data\Xml\XPath;
use WebStatic\Core\Content;
use WebStatic\Core\Template;

///////////////////////////////////////////////////////////////////////////////
// Home Template
// 

$homeTemplate = new Template( 'Home - Comming Soon', __DIR__ . '/home.phtml' );
$homeTemplate->Load();
$homeTemplate->Save();

$homeTemplate->Page_Jumbotron = '//div' . XPath::ContainsAttributeValue( 'class', 'jumbotron' );
$homeTemplate->Page_Jumbotron->Page_Jumbotron_Title = 'div[@class="container"]/h1';
$homeTemplate->Page_Jumbotron->Page_Jumbotron_SubTitle = 'div[@class="container"]/h3';

$homeTemplate->Page_UnderConstruction = '//div[@id="under-contruction"]';

$homeTemplate->Page_Container = '//div[@id="container"]';
$homeTemplate->Page_Container->Page_Row = 'div[@class="row"]';
$homeTemplate->Page_Container->Page_Row->Page_Column = 'div';
$homeTemplate->Page_Container->Page_Row->Page_Column->Home_Column_Icon = 'h2/span' . XPath::ContainsAttributeValue( 'class', 'glyphicon' );
$homeTemplate->Page_Container->Page_Row->Page_Column->Home_Column_Title = 'h2/span[2]';
$homeTemplate->Page_Container->Page_Row->Page_Column->Home_Column_Description = 'p[1]';
$homeTemplate->Page_Container->Page_Row->Page_Column->Home_Column_Details = 'p[2]';

$homeTemplate->Page_Footer = '//div[@id="footer"]';
$homeTemplate->Page_Footer->Page_Footer_Container = '//footer';
$homeTemplate->Page_Footer->Page_Footer_Container->Page_Footer_Copyright = 'span[1]';
$homeTemplate->Page_Footer->Page_Footer_Container->Page_Footer_Twitter_URL = 'span[2]';
$homeTemplate->Page_Footer->Page_Footer_Container->Page_Footer_Twitter_Title = 'span[2]';
$homeTemplate->Page_Footer->Page_Footer_Container->Page_Footer_GooglePlus_URL = 'span[3]';
$homeTemplate->Page_Footer->Page_Footer_Container->Page_Footer_GooglePlus_Title = 'span[3]';

function AddHomeColumn( Content $row, $class, $title )
{
    $column = $row->SetChild(
        'Page_Column', "$title - Column", null, null, $class );
    $column->SetChild(
        'Home_Column_Icon', "$title - Icon" );
    $column->SetChild(
        'Home_Column_Title', "$title - Title", null, $title );
    $column->SetChild(
        'Home_Column_Description', "$title - Description" );
    $column->SetChild(
        'Home_Column_Details', "$title - Details Button", 'a', "$title Details" );
}