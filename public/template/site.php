<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 22.05.2014
 * File: site.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

///////////////////////////////////////////////////////////////////////////////
// Site Template
// 

use AppStatic\Data\Xml\XPath;
use WebStatic\Core\Template;

$siteTemplate = new Template( 'Seattle - Comming Soon', __DIR__ . '/site.phtml' );
$siteTemplate->Load();
$siteTemplate->Save();

// Create a TemplateItem for the Menu.
$siteTemplate->Menu = '//ul[@class="nav navbar-nav"]';

$siteTemplate->Menu->MenuItem = 'li[1]';
$siteTemplate->Menu->MenuItem->MenuItem_Link = 'a';
$siteTemplate->Menu->MenuItem->MenuItem_Link->MenuItem_Icon = 'span' . XPath::ContainsAttributeValue( 'class', 'glyphicon' );
$siteTemplate->Menu->MenuItem->MenuItem_Link->MenuItem_Title = 'span[2]';

$siteTemplate->Menu->SubMenu = 'li[@class="dropdown"]';
$siteTemplate->Menu->SubMenu->SubMenu_Link = 'a[@class="dropdown-toggle"]';
$siteTemplate->Menu->SubMenu->SubMenu_Link->SubMenu_Icon = 'span' . XPath::ContainsAttributeValue( 'class', 'glyphicon' );
$siteTemplate->Menu->SubMenu->SubMenu_Link->SubMenu_Title = 'span[2]';

$siteTemplate->Menu->SubMenu->SubMenu_DropDown = 'ul[@class="dropdown-menu"]';
$siteTemplate->Menu->SubMenu->SubMenu_DropDown->SubMenuItem = 'li';
$siteTemplate->Menu->SubMenu->SubMenu_DropDown->SubMenuItem->SubMenuItem_Link = 'a';
$siteTemplate->Menu->SubMenu->SubMenu_DropDown->SubMenuItem->SubMenuItem_Link->SubMenuItem_Icon = 'span' . XPath::ContainsAttributeValue( 'class', 'glyphicon' );
$siteTemplate->Menu->SubMenu->SubMenu_DropDown->SubMenuItem->SubMenuItem_Link->SubMenuItem_Title = 'span[2]';

$siteTemplate->Menu->SubMenu->SubMenu_DropDown->SubMenuItem_Divider = 'li[@class="divider"]';
$siteTemplate->Menu->SubMenu->SubMenu_DropDown->SubMenuItem_Header = 'li[@class="dropdown-header"]';
$siteTemplate->Menu->SubMenu->SubMenu_DropDown->SubMenuItem_Header->SubMenuItem_Header_Icon = 'span' . XPath::ContainsAttributeValue( 'class', 'glyphicon' );
$siteTemplate->Menu->SubMenu->SubMenu_DropDown->SubMenuItem_Header->SubMenuItem_Header_Title = 'span[2]';

$siteTemplate->Menu->LanguageSelect = 'li[@id="language-select"]';
$siteTemplate->Menu->LanguageSelect->LanguageSelect_Toggle = 'a[@class="dropdown-toggle"]';
$siteTemplate->Menu->LanguageSelect->LanguageSelect_DropDown = 'ul[@class="dropdown-menu"]';
$siteTemplate->Menu->LanguageSelect->LanguageSelect_DropDown->LanguageSelectItem = 'li';
$siteTemplate->Menu->LanguageSelect->LanguageSelect_DropDown->LanguageSelectItem->LanguageSelectItem_Link = 'a';

$siteTemplate->LoginForm = '//form[@class="navbar-form navbar-right"]';
$siteTemplate->LoginForm->LoginForm_Email = 'div/input[@name="email"]';
$siteTemplate->LoginForm->LoginForm_Password = 'div/input[@name="pass"]';
$siteTemplate->LoginForm->LoginForm_SubmitButton = 'button[@id="sign-in"]';

$siteTemplate->ScrollToTop = '//ul' . XPath::ContainsAttributeValue( 'class', 'scroll-to-top' );