/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/gruntjs/grunt.d.ts" />
"use strict";
var grunt = require("grunt");
var Grunt;
(function (Grunt) {
    var GruntConfig = (function () {
        /**
         * @param _grunt
         * @param sourceDir
         * @param outDir
         * @param _outFile
         * @param tasks
         * @constructor
         */
        function GruntConfig(_grunt, sourceDir, outDir, _outFile) {
            if (_outFile === void 0) { _outFile = 'main'; }
            var tasks = [];
            for (var _i = 4; _i < arguments.length; _i++) {
                tasks[_i - 4] = arguments[_i];
            }
            this._grunt = _grunt;
            this._outFile = _outFile;
            /**
             * Task Store
             * @type {string[]}
             * @private
             */
            this.__ = ['time-grunt'];
            this._sourceDir = this.Dir('sourceDir', sourceDir);
            this._outDir = this.Dir('outDir', outDir);
            grunt.loadNpmTasks('grunt-extend-config');
            this.__ = this.__.concat(tasks);
            this._pkg = grunt.file.readJSON('package.json');
            this._bwr = grunt.file.readJSON('bower.json');
        }
        Object.defineProperty(GruntConfig.prototype, "outFile", {
            get: function () { return this._outFile; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GruntConfig.prototype, "pkg", {
            get: function () { return this._pkg; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GruntConfig.prototype, "bwr", {
            get: function () { return this._bwr; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GruntConfig.prototype, "_", {
            get: function () { return this.__; },
            set: function (value) { this.__ = this.__.concat(value); },
            enumerable: true,
            configurable: true
        });
        /**
         * Returns a function which prepends the specified dir to the file path specified at the returned function
         * and crates a string get property for the specified dir.
         * @param name
         * @param dir
         * @returns {(path:any)=>any} The full path of the specified file.
         * Path null -> it appends the *.js pattern to the directory.
         * Path '' => it returns the directory only.
         */
        GruntConfig.prototype.Dir = function (name, dir) {
            var map = function (_path) { return dir + _path; }, property = function (path) {
                return (path instanceof Array) ? path.map(map) : map((path || path === '') ? path : '*.js');
            };
            // Define property on this GruntConfig instance to enable usage
            // of the raw directory name within grunt template processing.
            Object.defineProperty(this, name, {
                get: function () {
                    return dir;
                },
                enumerable: true,
                configurable: true
            });
            return property;
        };
        GruntConfig.prototype.init = function () {
            require('load-grunt-tasks')(grunt, {
                config: './package.json',
                scope: 'devDependencies',
                requireResolution: true,
                pattern: ['grunt-contrib-*'].concat(this._)
            });
            grunt.initConfig(this);
        };
        /**
         * Extends the grunt configuration. Important: User only after GruntConfig.init()!
         * @param newConfig
         */
        GruntConfig.prototype.extend = function (newConfig) {
            this._grunt.extendConfig(newConfig);
        };
        /**
         * Add file banners.
         * @private
         */
        GruntConfig.prototype._banner = function (
            //js: string|string[] = [this._sourceDir()],
            ts) {
            if (ts === void 0) { ts = [this._sourceDir('*.ts')]; }
            this.banner = {
                //'php': ['<%= concat.dist.dest %>,<%= concat.dist.src %>']
                //'js': ['<%= concat.dist.src %>']
                //'js': ((js instanceof Array) ? js : [js]),
                'ts': ((ts instanceof Array) ? ts : [ts]),
            };
            require('./banner.js').banner(this);
        };
        /**
         * Copy bower components to distribution directory
         * @param options
         * @param targets
         * @private
         */
        GruntConfig.prototype._bowercopy = function (options, targets) {
            this._ = ['grunt-bowercopy'];
            this.bowercopy = { options: options };
            for (var _target in targets)
                this.bowercopy[_target] = { files: targets[_target] };
        };
        /**
         * Replace strings
         * @param taskName
         * @param src
         * @param dest
         * @param patterns
         * @private
         */
        GruntConfig.prototype._replace = function (taskName, src, dest, patterns) {
            if (!this.replace) {
                this._ = ['grunt-replace'];
                this.replace = {};
            }
            this.replace[taskName] = {
                options: {
                    preserveOrder: true,
                    patterns: (function () {
                        var config = [];
                        for (var _replace in patterns)
                            config.push({
                                match: patterns[_replace],
                                replacement: _replace
                            });
                        return config;
                    })()
                },
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ((src instanceof Array) ? src : [src]),
                        dest: dest
                    }
                ]
            };
        };
        /**
         * Wrap whole file contents
         * @param taskName
         * @param src
         * @param dest
         * @param prepend
         * @param append
         * @private
         */
        GruntConfig.prototype._wrap = function (taskName, src, dest, prepend, append) {
            if (!this.wrap) {
                this._ = ['grunt-wrap'];
                this.wrap = {};
            }
            this.wrap[taskName] = {
                src: ((src instanceof Array) ? src : [src]),
                dest: dest,
                options: {
                    wrapper: [prepend, append]
                }
            };
        };
        /**
         * Convert commonJS files into amd modules.
         * @param taskName
         * @param src
         * @param dest
         * @param exports
         * @param imports
         * @private
         */
        GruntConfig.prototype._wrap_amd = function (taskName, src, dest, exports, imports) {
            if (imports === void 0) { imports = ''; }
            this._wrap(taskName, src, dest, 'define(function (require, exports, module) {' + imports + '\n', '\nreturn ' + exports + ';\n});');
        };
        /**
         * Compress modules and concatenated code
         * @private
         */
        GruntConfig.prototype._minify = function (minify) {
            this.minify = minify;
            require('./minify.js').minify(this, {
                dest: this._outDir(''),
                map: true
            });
        };
        /**
         * Check for JavaScript errors.
         */
        GruntConfig.prototype._jshint = function (files) {
            if (files === void 0) { files = [this._sourceDir()]; }
            this.jshint = {
                files: ((files instanceof Array) ? files : [files]),
                options: {
                    globals: {
                        jQuery: true,
                        console: true,
                        module: true
                    },
                    asi: true // Ignore semicolon warnings
                }
            };
        };
        /**
         * Style checker definitions (optional)
         * @private
         */
        GruntConfig.prototype._jscs = function (files) {
            if (files === void 0) { files = [this._sourceDir()]; }
            this.jscs = {
                files: ((files instanceof Array) ? files : [files]),
                options: {
                    preset: "jquery"
                }
            };
        };
        /**
         * Concat all modules.
         * @private
         */
        GruntConfig.prototype._concat = function (src, dest) {
            if (dest === void 0) { dest = this._outDir(this.outFile + '.js'); }
            this.concat = {
                options: {
                    separator: '\r\n\r\n',
                    stripBanners: { block: true },
                    sourceMap: true
                },
                dist: {
                    src: src,
                    dest: dest,
                    nonull: true
                }
            };
        };
        /**
         * Observe source files for changes and regenerate
         * @private
         */
        GruntConfig.prototype._watch = function (files, tasks) {
            if (files === void 0) { files = ['<%= jshint.files %>']; }
            if (tasks === void 0) { tasks = ['jshint', 'concat', 'minify']; }
            this.watch = {
                files: ((files instanceof Array) ? files : [files]),
                tasks: ((tasks instanceof Array) ? tasks : [tasks])
            };
        };
        /**
         *
         * @private
         */
        GruntConfig.prototype._concurrent = function (checkTasks, buildTasks) {
            if (checkTasks === void 0) { checkTasks = ['jshint']; }
            if (buildTasks === void 0) { buildTasks = ['concat', 'minify', 'watch']; }
            this._ = ['grunt-concurrent'];
            this.concurrent = {
                options: {
                    logConcurrentOutput: true
                },
                check: {
                    tasks: ((checkTasks instanceof Array) ? checkTasks : [checkTasks])
                },
                build: {
                    tasks: ((buildTasks instanceof Array) ? buildTasks : [buildTasks])
                }
            };
        };
        /**
         * Generates a single index.d.ts file for your NPM package implemented in TypeScript
         * by concatenating per-file d.ts files, wrapping them all into an implicit module
         * declaration and rewriting/moving some lines.
         * @param taskName
         * @param src
         * @param dest
         * @private
         */
        GruntConfig.prototype._typescript_export = function (taskName, src, dest) {
            if (taskName === void 0) { taskName = this.outFile; }
            if (src === void 0) { src = [this._sourceDir('*.d.ts')]; }
            if (dest === void 0) { dest = this._outDir(this.outFile + '.d.ts'); }
            if (!this.typescript_export) {
                this._ = ['grunt-typescript-export'];
                this.typescript_export = {};
            }
            this.typescript_export[taskName] = {
                src: ((src instanceof Array) ? src : [src]),
                dest: dest
            };
        };
        /**
         *
         * @see https://github.com/gruntjs/grunt-contrib-compass
         * @param env
         * @param options
         * @private
         */
        GruntConfig.prototype._compass = function (env, options) {
            if (options === void 0) { options = {}; }
            var defaultOptions = {
                force: true,
                config: 'compass/config.rb',
                sassDir: 'compass/sass/',
                httpPath: '../',
                cssDir: 'css',
                fontsDir: 'fonts',
                imagesDir: 'img',
                outputStyle: 'compact',
                environment: 'development'
            };
            if (!this.compass)
                this.compass = {};
            this.compass[env] = {
                options: (function () {
                    // extend options parameter with default values
                    for (var _target in defaultOptions) {
                        if (!options.hasOwnProperty(_target))
                            options[_target] = defaultOptions[_target];
                    }
                    if (options.hasOwnProperty('specify'))
                        options['specify'] = options['specify'].map(function (path) { return options['sassDir'] + path; });
                    options['environment'] = env;
                    return options;
                })()
            };
        };
        GruntConfig.prototype._auto_install = function (taskName, cwd, npm) {
            if (npm === void 0) { npm = '--development'; }
            if (!this.auto_install) {
                this._ = ['grunt-auto-install'];
                this.auto_install = {};
                this.auto_install['local'] = {};
            }
            this.auto_install[taskName] = {
                options: {
                    cwd: cwd,
                    stdout: true,
                    stderr: true,
                    failOnError: true,
                    npm: npm
                }
            };
        };
        GruntConfig.prototype._run_grunt = function (taskName, src, tasks) {
            if (!this.run_grunt) {
                this._ = ['grunt-run-grunt'];
                this.run_grunt = {};
                this.run_grunt['options'] = {
                    minimumFiles: 1
                };
            }
            this.run_grunt[taskName] = {
                src: ((src instanceof Array) ? src : [src]),
                options: {
                    task: tasks,
                    log: false,
                    process: function (res) {
                        if (res.fail) {
                            res.output = 'new content';
                            grunt.log.writeln('Grunt compile failed!');
                        }
                    }
                },
            };
        };
        return GruntConfig;
    }());
    Grunt.GruntConfig = GruntConfig;
})(Grunt || (Grunt = {}));
module.exports = Grunt;
//# sourceMappingURL=GruntConfig.js.map