/// <reference path="gruntjs.d.ts" />

declare namespace grunt {
    namespace config {}
    namespace event {}
    namespace file {
        interface FilesConfig extends IFilesConfig {
            orig: grunt.file.IFilesConfig
        }
    }
    namespace log {}
    namespace option {}
    namespace task {
        interface ITask {
            options<T extends ITaskOptions|any>(defaultsObj?: T): T
        }
    }
    namespace template {}
    namespace util {}
}
import ProjectConfig = grunt.config.IProjectConfig;
declare interface grunt extends IGrunt
{
    /**
     * Extends the grunt configuration. Use GruntConfig.extend<T>
     * @param newConfig
     */
    extendConfig: <T extends ProjectConfig>(newConfig: T)=>void
}