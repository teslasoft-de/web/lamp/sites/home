/// <reference path="typings/node/node.d.ts" />
/// <reference path="typings/gruntjs/grunt.d.ts" />

module Grunt
{
    export class Vendor
    {
        constructor(grunt: grunt)
        {
            const config = new (require('./GruntConfig').GruntConfig)(
                grunt, 'vendor/', 'dist/', 'vendor'),
                vendor = config._sourceDir,
                bower = config.Dir('bower', 'bower_components/');

            config._bowercopy({
                destPrefix: vendor('../../'),
                // Remove downloaded bower components after copying dist contents
                //clean: true
            }, {
                jquery:{
                    'js/vendor/jquery.js': 'jquery/dist/jquery.js',
                    'js/vendor/jquery.min.js': 'jquery/dist/jquery.min.js'
                },
                jquery_migrate:{
                    'js/vendor/jquery-migrate.js': 'jquery-migrate/jquery-migrate.js',
                    'js/vendor/jquery-migrate.min.js': 'jquery-migrate/jquery-migrate.min.js'
                },
                jquery_cookie:{
                    'js/vendor/jquery.cookie.js': 'jquery.cookie/jquery.cookie.js'
                },
                jquery_easing:{
                    'js/vendor/jquery.easing.js': 'jquery-easing/jquery.easing.js',
                    'js/vendor/jquery.easing.min.js': 'jquery-easing/jquery.easing.min.js'
                },
                scroll_depth:{
                    'js/vendor/jquery.scrolldepth.js': 'scroll-depth/jquery.scrolldepth.js',
                    'js/vendor/jquery.scrolldepth.min.js': 'scroll-depth/jquery.scrolldepth.min.js'
                },
                fontawesome          : {
                    'fonts/'                                                         : 'font-awesome/fonts/',
                    'compass/sass/fonts/vector-icons/font-awesome/'                  : 'font-awesome/scss/_*',
                    'compass/sass/fonts/vector-icons/font-awesome/_font-awesome.scss': 'font-awesome/scss/font-awesome.scss'
                },
                elusiveicons         : {
                    'fonts/'                                                           : 'elusive-icons/fonts/',
                    'compass/sass/fonts/vector-icons/elusive-icons/'                   : 'elusive-icons/scss/_*',
                    'compass/sass/fonts/vector-icons/elusive-icons/_elusive-icons.scss': 'elusive-icons/scss/elusive-icons.scss'
                },
                bootstrap_sass       : {
                    'fonts/'                                    : 'bootstrap-sass/assets/fonts/',
                    'compass/sass/vendor/bootstrap/stylesheets/': 'bootstrap-sass/assets/stylesheets/'
                },
                /*bootstrap_bootswatch: {

                    '../compass/sass/vendor/bootstrap/stylesheets/bootstrap/_variables.scss'                  : 'bootswatch/superhero/_variables.scss',
                    '../compass/sass/vendor/bootstrap/stylesheets/bootstrap/themes/superhero/_bootswatch.scss': 'bootswatch/superhero/_bootswatch.scss'
                },*/
                bootstrap_select     : {
                    'compass/sass/vendor/bootstrap/plugins/_bootstrap-select.scss': 'bootstrap-select/sass/bootstrap-select.scss',
                    'compass/sass/vendor/bootstrap/plugins/_variables.scss'       : 'bootstrap-select/sass/variables.scss',
                    'js/vendor/bootstrap-select.js'                               : 'bootstrap-select/dist/js/bootstrap-select.js',
                    'js/vendor/bootstrap-select.min.js'                           : 'bootstrap-select/dist/js/bootstrap-select.min.js'
                },
                bootstrap_maxlength  : {
                    'js/vendor/bootstrap-maxlength.js':'bootstrap-maxlength/src/bootstrap-maxlength.js'
                },
                responsejs           :{
                    'js/vendor/response.js'    : 'responsejs/response.js',
                    'js/vendor/response.min.js': 'responsejs/response.min.js'
                },
                sly                  :{
                    'js/vendor/sly.js'    : 'sly/dist/sly.js',
                    'js/vendor/sly.min.js': 'sly/dist/sly.min.js'
                }
            });

            //config._compass('development', {outputStyle: 'compact', specify: ['screen.scss']});
            //config._compass('production', {outputStyle: 'compressed', specify: ['screen.scss']});

            config._jshint([
                'vendor/jquery.cookie.js',
                'vendor/vague.js']);
            config._jscs();

            config._minify({
                'jquery.cookie'      : [vendor('jquery.cookie.js')],
                'vague'              : [vendor('vague.js')],
                'bootstrap-maxlength': [vendor('bootstrap-maxlength.js')]
            });
            config.minify.options.dest = vendor('');

            config._concat(config._sourceDir([
                'jquery-migrate.js',
                'jquery.mobile.custom.js',
                'jquery.easing.js',
                'jquery.cookie.js',
                'bootstrap.js',
                'response.js',
                'vague.js',
                'sly.js']));
            config.concat.min = {
                src: config._sourceDir([
                    'jquery-migrate.min.js',
                    'jquery.mobile.custom.min.js',
                    'jquery.easing.min.js',
                    'jquery.cookie.min.js',
                    'bootstrap.min.js',
                    'response.min.js',
                    'vague.min.js',
                    'sly.min.js']),
                dest: config._outDir(config.outFile + '.min.js'),
                nonull: true
            };

            config._watch();

            config.init();

            grunt.registerTask('build_dependencies', ['bowercopy']);

            grunt.registerTask('build', ['jshint', 'minify', 'concat', 'watch']);
        }
    }
}
export = Grunt.Vendor;