/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.10.2014
 * File: redirect.php
 * Encoding: UTF-8
 * Project: Teslasoft
 **/

// Toggle sign in login form controls if no value present.
$('#sign-in').on('click', function(){
    var c = $('#slide-nav .form-control').stop(true, true);
    if(!$('#slide-nav .form-control').val()){
        c.slideToggle(300);
        return false;
    }
});
$('#slide-nav .form-control').hide();