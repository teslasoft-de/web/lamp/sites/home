/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.10.2014
 * File: redirect.php
 * Encoding: UTF-8
 * Project: Teslasoft
 **/

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 */

var slyFrame = $('#scrollspy');
var slyWrap = slyFrame.parent();
var slyScroll = slyWrap.find('.scrollspy.scrollbar');
var activeItem = '[id|="scrollspy"] .nav li[class*="active"]:not(.active-fixed)';
var sidebarNav = '#scrollspy-sidebar ul.nav';
var slideNav = $('#slide-nav');
var slyDefaultOptions = {
    horizontal: 1,
    itemNav: 'centered',
    smart: 1,
    activateOn: 'click',
    mouseDragging: 1,
    touchDragging: 1,
    releaseSwing: 1,
    //startAt: 1,
    scrollBar: slyScroll,
    scrollBy: 1,
    speed: 300,
    swingSpeed: 0.3,
    elasticBounds: 1,
    easing: 'easeOutExpo',
    dragHandle: 1,
    dynamicHandle: 1,
    minHandleSize: 25,
    clickBar: 1,
    activeClass: ''
    // Buttons
    //prev: $wrap.find('.prev'),
    //next: $wrap.find('.next')
};
var sly;
var slyActive = typeof Sly ==='function' && slyFrame.length;
if(slyActive)
    $.extend(Sly.defaults, slyDefaultOptions);
slyFrame.find('.nav').width(4096);

var scrollOffset = 21;
var computeScrollOffset = function(scroll){
    var offset = slideNav.height() + scrollOffset;
    return scroll ? -offset +1 : offset;
};

// Suspend scroll to anchor while ScollSpy is updating.
var scrollDelay = 0;
smoothScroll.delayCallback = function(){
    return scrollDelay;
};

function toggleSly() {
    if(!sly)
        return;
    if(sly.initialized)
        sly.reload();
    else
        sly = sly.destroy().init();

    var handle = slyScroll.find('.handle');
    var maxWidth = handle.closest('.container').innerWidth();
    var itemsWidth = 0;
    $('#scrollspy li:visible').each(function() {
        itemsWidth += $(this).outerWidth(true);
    });
    // Pixel rounding bugfix. (Sometimes one pixel from items is missing under jquery.)
    itemsWidth +=1;
    
    if (handle.width() == maxWidth || itemsWidth <= maxWidth) {
        var active = $(activeItem);
        sly = sly.destroy();
        active.addClass('active');
        slyScroll.fadeOut(300, function(){
            // Resume scroll to anchor
            scrollDelay = 0;
        });
    }
    else {
        slyScroll.fadeIn(300, function(){
            refreshScrollSpy();
            // Resume scroll to anchor
            scrollDelay = 0;
        });
        if(!sly.initialized)
            sly = sly.init();
        else
            sly = sly.destroy().init();   
    }
    // Apply pixel rounding bugfix.
    slyFrame.find('ul').width(itemsWidth);
}

var resetScrollContainer = function(){
    // Clip ScrollSpy height to item height to prevent item wrapping.
    slyFrame.height(slyFrame.find('li').height());
    // Reset width to prevent item wrapping.
    slyFrame.find('ul').width(4096);
};
resetScrollContainer();

function updateScrollSpy(fade) {
    // Suspend scroll to anchor
    scrollDelay = 50;
    var fadeDuration = fade ? 300 : 0;
    var links = $('#scrollspy li a');
    
    resetScrollContainer();
    
    // Hide all anchor links with hidden targets.
    links.each(function(i, o) {
        var a = $(o);
        if ($(a.attr('href')).is(':hidden')){
            a.parent().fadeOut(fadeDuration, function(){
                $(this).css({width: 0});
            });
        }
    });
    // Show all anchor links with visible targets.
    setTimeout(function() {
        links.each(function(i, o) {
            var a = $(o);
            if (!$(a.attr('href')).is(':hidden')){
                a.parent().css({width: 'auto'});
                a.parent().fadeIn(fadeDuration);
            }
        });
        
        // Update sly
        setTimeout(function() {
            toggleSly();
        }, fadeDuration);
    }, fadeDuration);
}

/**
 * Refreshes all Bootstrap ScrollSpy instances and adjusts the offset corresponding to the slide-nav.
 */
function refreshScrollSpy()
{
    $('[data-spy="scroll"]').each(function () {
        // Adjust offset from navbar-fixed-top to scrollspy.
        $(this).data()['bs.scrollspy'].options.offset = computeScrollOffset(false);
        // Set the new offset
        $(this).data()['bs.scrollspy'].process();
        // Force scrollspy to recalculate the offsets to your targets
        $(this).scrollspy('refresh'); // Refresh the scrollspy.
    });
}

// Observe slide-nav height change, refresh ScrollSpy and adjust affix top offset.
var slideNavHeight = 0;
function monitorScrollSpy()
{
    if (slideNav.css('height') != slideNavHeight)
    {
        slideNavHeight = slideNav.css('height');
        window.refreshScrollSpy();
        $('ul.nav-tabs').css('top', slideNav.height() + 10);
        // TODO: Create event and update site content in corresponding plugins.
        $('#page-contents').css('padding-top', slideNav.height());
        if($('.jumbotron').data('parallax'))
            $('.jumbotron').parallax();
    }
    
}

$(document).ready(function() {
    // Adjust affix top offset to slide-nav height.
    // Get initial affix offset and substract slide-nav height.
    scrollSpyOffsetTop = $(sidebarNav).attr('data-offset-top') - slideNav.height();
    $(sidebarNav).on('affix.bs.affix', function(e) {
        if (!Response.band(768))
            return false;
        // Add current slide-nav height to initial affix offset.
        var offset = scrollSpyOffsetTop + slideNav.height();
        if (offset == $(this).attr('data-offset-top'))
            return;

        // Do events, delete and create affix and refresh scoll spy. (Has currently on Bootstrap 3.2.0 no effect.)
        setTimeout(function() {
            $(sidebarNav).removeData('affix').removeClass('affix affix-top affix-bottom');
            $(sidebarNav).attr('data-offset-top', offset);
            $(sidebarNav).affix({
                offset: {
                    top: function() {
                        return offset;
                    }
                }
            });
            $('body').scrollspy('refresh');
        }, 0);
        return false;
    });
    
    // Remember active slide-nav menu items when scrollspy activating affix menu items.
    var activeSlideNavItems = $('#slide-nav li.active');
    $('[id|="scrollspy"] ul.nav').on('activate.bs.scrollspy', function(e) {
        activeSlideNavItems.addClass('active');
        activeSlideNavItems.addClass('active-fixed');
        if(slyActive)
            sly.activate($(activeItem).index());
    });

    // Initialize and deselect auto selected item.
    if(slyActive)
        sly = new Sly(slyFrame).init();
    $(activeItem).removeClass('active');

    // Update sly on window resize
    $(window).on('throttledresize', function() {
        toggleSly();
    });

    // Add offset from navbar-fixed-top to scrollspy for affix.
    $('body').scrollspy({offset: computeScrollOffset(false)});
    setInterval(monitorScrollSpy, 500);
    
    // Setup smooth anchor scrolling
    $("[id|='scrollspy'] li a, a[class$='-link']").not('.sitemap-link').on('click', smoothScroll.hashLinkClicked);
    smoothScroll.topScrollOffset = function() { return computeScrollOffset(true); };
});