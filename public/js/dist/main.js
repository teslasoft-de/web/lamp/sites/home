//#############################################################################
// Browser detection and viewport adjustments
//#############################################################################

function getAndroidVersion(userAgent) {
    var ua = userAgent || navigator.userAgent;
    var match = ua.match(/Android\s([\d\.]*)/);
    return match ? match[1].split('.') : false;
}
var androidVersion = getAndroidVersion();

// Disable zoom on all mobile devices

// Chrome & Android >= 4
if(!!window.chrome || androidVersion && (androidVersion[0] >= 4))
    $('meta[name=viewport]').attr('content','initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no');
// Android <= 4
else if(androidVersion && (androidVersion[0] < 4))
    $('meta[name=viewport]').attr('content','initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,height=device-height,target-densitydpi=device-dpi,user-scalable=yes');
// IE 10
else if (/IEMobile\/10\.0/.test( navigator.userAgent )) {
    // Workaround for Internet Explorer 10 doesn't differentiate device width from viewport width.
    var msViewportStyle = document.createElement('style');
    msViewportStyle.appendChild(
        document.createTextNode(
            '@-ms-viewport{width:auto!important}'
        )
    );
    document.querySelector('head').appendChild(msViewportStyle);
}
// iOS/Safari
else if(/(iPad|iPhone|iPod)/g.test( navigator.userAgent )){
    $('meta[name=viewport]').attr('content','initial-scale=1.0,width=device-width,user-scalable=0');
}

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 */

var slyFrame = $('#scrollspy');
var slyWrap = slyFrame.parent();
var slyScroll = slyWrap.find('.scrollspy.scrollbar');
var activeItem = '[id|="scrollspy"] .nav li[class*="active"]:not(.active-fixed)';
var sidebarNav = '#scrollspy-sidebar ul.nav';
var slideNav = $('#slide-nav');
var slyDefaultOptions = {
    horizontal: 1,
    itemNav: 'centered',
    smart: 1,
    activateOn: 'click',
    mouseDragging: 1,
    touchDragging: 1,
    releaseSwing: 1,
    //startAt: 1,
    scrollBar: slyScroll,
    scrollBy: 1,
    speed: 300,
    swingSpeed: 0.3,
    elasticBounds: 1,
    easing: 'easeOutExpo',
    dragHandle: 1,
    dynamicHandle: 1,
    minHandleSize: 25,
    clickBar: 1,
    activeClass: ''
    // Buttons
    //prev: $wrap.find('.prev'),
    //next: $wrap.find('.next')
};
var sly;
var slyActive = typeof Sly ==='function' && slyFrame.length;
if(slyActive)
    $.extend(Sly.defaults, slyDefaultOptions);
slyFrame.find('.nav').width(4096);

var scrollOffset = 21;
var computeScrollOffset = function(scroll){
    var offset = slideNav.height() + scrollOffset;
    return scroll ? -offset +1 : offset;
};

// Suspend scroll to anchor while ScollSpy is updating.
var scrollDelay = 0;
smoothScroll.delayCallback = function(){
    return scrollDelay;
};

function toggleSly() {
    if(!sly)
        return;
    if(sly.initialized)
        sly.reload();
    else
        sly = sly.destroy().init();

    var handle = slyScroll.find('.handle');
    var maxWidth = handle.closest('.container').innerWidth();
    var itemsWidth = 0;
    $('#scrollspy li:visible').each(function() {
        itemsWidth += $(this).outerWidth(true);
    });
    // Pixel rounding bugfix. (Sometimes one pixel from items is missing under jquery.)
    itemsWidth +=1;
    
    if (handle.width() == maxWidth || itemsWidth <= maxWidth) {
        var active = $(activeItem);
        sly = sly.destroy();
        active.addClass('active');
        slyScroll.fadeOut(300, function(){
            // Resume scroll to anchor
            scrollDelay = 0;
        });
    }
    else {
        slyScroll.fadeIn(300, function(){
            refreshScrollSpy();
            // Resume scroll to anchor
            scrollDelay = 0;
        });
        if(!sly.initialized)
            sly = sly.init();
        else
            sly = sly.destroy().init();   
    }
    // Apply pixel rounding bugfix.
    slyFrame.find('ul').width(itemsWidth);
}

var resetScrollContainer = function(){
    // Clip ScrollSpy height to item height to prevent item wrapping.
    slyFrame.height(slyFrame.find('li').height());
    // Reset width to prevent item wrapping.
    slyFrame.find('ul').width(4096);
};
resetScrollContainer();

function updateScrollSpy(fade) {
    // Suspend scroll to anchor
    scrollDelay = 50;
    var fadeDuration = fade ? 300 : 0;
    var links = $('#scrollspy li a');
    
    resetScrollContainer();
    
    // Hide all anchor links with hidden targets.
    links.each(function(i, o) {
        var a = $(o);
        if ($(a.attr('href')).is(':hidden')){
            a.parent().fadeOut(fadeDuration, function(){
                $(this).css({width: 0});
            });
        }
    });
    // Show all anchor links with visible targets.
    setTimeout(function() {
        links.each(function(i, o) {
            var a = $(o);
            if (!$(a.attr('href')).is(':hidden')){
                a.parent().css({width: 'auto'});
                a.parent().fadeIn(fadeDuration);
            }
        });
        
        // Update sly
        setTimeout(function() {
            toggleSly();
        }, fadeDuration);
    }, fadeDuration);
}

/**
 * Refreshes all Bootstrap ScrollSpy instances and adjusts the offset corresponding to the slide-nav.
 */
function refreshScrollSpy()
{
    $('[data-spy="scroll"]').each(function () {
        // Adjust offset from navbar-fixed-top to scrollspy.
        $(this).data()['bs.scrollspy'].options.offset = computeScrollOffset(false);
        // Set the new offset
        $(this).data()['bs.scrollspy'].process();
        // Force scrollspy to recalculate the offsets to your targets
        $(this).scrollspy('refresh'); // Refresh the scrollspy.
    });
}

// Observe slide-nav height change, refresh ScrollSpy and adjust affix top offset.
var slideNavHeight = 0;
function monitorScrollSpy()
{
    if (slideNav.css('height') != slideNavHeight)
    {
        slideNavHeight = slideNav.css('height');
        window.refreshScrollSpy();
        $('ul.nav-tabs').css('top', slideNav.height() + 10);
        // TODO: Create event and update site content in corresponding plugins.
        $('#page-contents').css('padding-top', slideNav.height());
        if($('.jumbotron').data('parallax'))
            $('.jumbotron').parallax();
    }
    
}

$(document).ready(function() {
    // Adjust affix top offset to slide-nav height.
    // Get initial affix offset and substract slide-nav height.
    scrollSpyOffsetTop = $(sidebarNav).attr('data-offset-top') - slideNav.height();
    $(sidebarNav).on('affix.bs.affix', function(e) {
        if (!Response.band(768))
            return false;
        // Add current slide-nav height to initial affix offset.
        var offset = scrollSpyOffsetTop + slideNav.height();
        if (offset == $(this).attr('data-offset-top'))
            return;

        // Do events, delete and create affix and refresh scoll spy. (Has currently on Bootstrap 3.2.0 no effect.)
        setTimeout(function() {
            $(sidebarNav).removeData('affix').removeClass('affix affix-top affix-bottom');
            $(sidebarNav).attr('data-offset-top', offset);
            $(sidebarNav).affix({
                offset: {
                    top: function() {
                        return offset;
                    }
                }
            });
            $('body').scrollspy('refresh');
        }, 0);
        return false;
    });
    
    // Remember active slide-nav menu items when scrollspy activating affix menu items.
    var activeSlideNavItems = $('#slide-nav li.active');
    $('[id|="scrollspy"] ul.nav').on('activate.bs.scrollspy', function(e) {
        activeSlideNavItems.addClass('active');
        activeSlideNavItems.addClass('active-fixed');
        if(slyActive)
            sly.activate($(activeItem).index());
    });

    // Initialize and deselect auto selected item.
    if(slyActive)
        sly = new Sly(slyFrame).init();
    $(activeItem).removeClass('active');

    // Update sly on window resize
    $(window).on('throttledresize', function() {
        toggleSly();
    });

    // Add offset from navbar-fixed-top to scrollspy for affix.
    $('body').scrollspy({offset: computeScrollOffset(false)});
    setInterval(monitorScrollSpy, 500);
    
    // Setup smooth anchor scrolling
    $("[id|='scrollspy'] li a, a[class$='-link']").not('.sitemap-link').on('click', smoothScroll.hashLinkClicked);
    smoothScroll.topScrollOffset = function() { return computeScrollOffset(true); };
});

//#############################################################################
// Navbar
//#############################################################################

// Add slidedown animation to site menu dropdowns
$(document).ready(function() {
    var $dropDown = $('.navbar .dropdown'),
        $toggle = $('.navbar .dropdown-toggle');

    // Initialize dropdown plugin
    $toggle.dropdown();

    // Handle menu dropdown click events.
    $dropDown.on('show.bs.dropdown', function(e) {
        var $dropdown = $(this);
        $dropdown.find('.dropdown-menu').first()
            .stop(true, true).slideToggle(400, function () {
                $toggle.each(function (i, e) {
                    var $e = $(e);
                    var $parent = $e.closest('.dropdown');
                    if (!$parent.is($dropdown) && $parent.is('.open'))
                        $e.trigger('click');
                });
            });
    });

    // Remove .open class after animation complete to prevent showing desktop
    // styles in responsive view.
    $dropDown.on('hide.bs.dropdown', function () {
        var $dropDown = $(this),
            $menu = $dropDown.find('.dropdown-menu').first();
        $menu.stop(true, true).slideUp(300, function () {
            var $parent = $dropDown.closest('.open');
            $parent.removeClass('open');
            $menu.css('display', 'none');
        });

        // Prevent bootstrap from removing .open class immediately after click.
        return false;
    });

    // Close all menus on content click.
    $('#page-contents').on('mousedown', function(e) {
        $toggle.each( function( i, e ){
            var $e = $(e);
            var $parent = $e.closest('.dropdown');
            if($parent.is( '.open' ))
                $e.dropdown('toggle');
        });
    });
});

/**
 * Disable user selection and display menu glyphicons in responsive view.
 */
function changeMenuResponsiveStyle()
{
    if ( Response.band(768) )
        $('#slide-nav.navbar').enableSelection();
    else
        $('#slide-nav.navbar').disableSelection();
}
changeMenuResponsiveStyle();

$(window).on('throttledresize', function() {
    if ( Response.band(1200) )
    {
        // 1200+
    }
    else if ( Response.band(992) )
    {
        // 992+
    }
    else if ( Response.band(768) )
    {
        // 768+
    }
    else
    {

    }
    changeMenuResponsiveStyle();
});

// Toggle sign in login form controls if no value present.
$('#sign-in').on('click', function(){
    var c = $('#slide-nav .form-control').stop(true, true);
    if(!$('#slide-nav .form-control').val()){
        c.slideToggle(300);
        return false;
    }
});
$('#slide-nav .form-control').hide();

/**
 * Created by Cosmo on 15.10.2014.
 */
// Check if the cookie hint has been closed.
if( $.cookie('cookie-hint') === 'closed' )
    $('#cookie-hint').hide();

// Set cookie and remember for 7 days that the cookie hint has been closed.
$('#cookie-hint button').click(function( e ){
    e.preventDefault();
    $.cookie('cookie-hint', 'closed', { path: '/', expires: 7 });
});

// Experimental pager header minimum height.
/*var setPageHeaderMinHeight = function(){
    $('.page-header').css('min-height', $(window).height() - $('slide-nav').height());
};
setPageHeaderMinHeight();
$(document).on('throttledresize', setPageHeaderMinHeight);*/

$(document).ready(function() {
    // TODO: Extend so also child page items will open.
    // Trigger hash id link clicks so corresponding collapsed page headers will open.
    var hashID = smoothScroll.getNavIdFromHash();
    if(hashID.substring(0,1) == '#')
        hashID = hashID.substring(1);

    // Searches for a page toggler link element and clicks the declared click target.
    var findPageToggler = function(hashID){
        var link = $("a[href='#"+hashID+"'].scrollspy-link");
        if(link.length){
            var target = link.find(link.attr('data-target'));
            if($('#'+hashID).length && target.length){
                target.click();
                return true;
            }
        }
        return false;
    };

    // If no page toggler found, toggle the hash id element's parent page hader and scroll to hash id.
    if(!findPageToggler(hashID)){
        target = $("[id='"+hashID+"'].scrollspy-target");
        if(target.length){
            target = target.closest('.page-header');
            var match = target.attr("class").match(/page-header-toggle-([\w-]+)\b/);
            if(match){
                togglePageHeaders(match[1]);
                smoothScroll.scrollToElement('#'+hashID);
                return;
            }
        }
        smoothScroll.loaded();
    }
});

// TODO: Open pages from child anchor calls
/* Collapsing Page Headers */
if(!$(smoothScroll.getNavIdFromHash()).length)
    $("[class|='page-header-collapse'], [class*=' page-header-toggle-']").hide();
updateScrollSpy(false);

function togglePageHeaders( id ){
    $("[class='page-header-toggle-"+id+"'], [class~='page-header-toggle-"+id+"']").each( function(i,o){
        $(o).show();
    });
    $("[class|='page-header-collapse']:not(.page-header-toggle-"+id+"), [class*=' page-header-toggle-']:not(.page-header-toggle-"+id+")").each( function(i,o){
        $(o).hide();
    });
    toggleLastPageHeader();
    setTimeout( function(){
        updateScrollSpy(true);
    },0);
}
function toggleLastPageHeader(){
    $('.page-header-last').removeClass('page-header-last');
    $('.page-header').not(':hidden').last().addClass('page-header-last');
}
toggleLastPageHeader();

$('.img-dropshadow-toggle').on('click',function(){
    $(this).addClass( 'img-dropshadow' );
    $('.img-dropshadow-toggle:not(#'+$(this).attr('id')+')').removeClass('img-dropshadow');
});

/**
 * Created by Cosmo on 15.10.2014.
 */
$(document).ready(function() {

    // On open modal dialog wait for menu collapse animation to prevent simultanous triggered animations.
    $('.navbar .container:eq(0) .nav li > a[href^=#]').each( function( i, e ){
        var a = $(e);
        var href = a.attr('href');
        if(!$('div[id='+href+']'))
            return;
        a.attr('href', null);

        // Set dialog label
        var id = href.substring(1) + 'Label';
        a.attr( 'id', id );
        $(href).attr('aria-labelledby', id);

        a.on( 'click', function() {
            setTimeout( function() {
                $(href).modal();
                window.document.location = href;
            }, 300 );
        });
    } );

    // Blur page content while dialog shown.
    $('.modal').on('show.bs.modal', function(e) {
        if(!dialogSwitch)
            blur('#page-contents', true);
    }).on('hide.bs.modal', function(e) {
        if(!dialogSwitch && Response.band(768))
            blur('#page-contents', false);
    });

    // Enable dialog to dialog opening.
    dialogSwitch = false;
    $('.modal-body a[href^=#]').on('click', function(e){
        var a = $(this);
        var dialogID = a.attr('href');
        // Cancel if link target is not element id.
        if(!$(dialogID).hasClass('modal'))
            return;
        dialogSwitch = true;
        a.closest('.modal').one('hidden.bs.modal', function(e) {
            setTimeout( function(){
                $(dialogID).modal();
                window.document.location = dialogID;
                dialogSwitch = false;
            }, 0 );

        }).modal('hide');
    });
});

// Prevent navbar move while dialog opened.
// Content move has been fixed in Bootstrap 3.2.0.
// -->  Open modal is shifting body content to the left #9855
$(document.body)
    .on('show.bs.modal', function () {
        if (this.clientHeight <= window.innerHeight) {
            return;
        }
        // Get scrollbar width
        var scrollbarWidth = getScrollBarWidth();
        if (scrollbarWidth) {
            $('.navbar-fixed-top').css('padding-right', scrollbarWidth);
        }
    })
    .on('hide.bs.modal', function () {
        $('.navbar-fixed-top').css('padding-right', 0);
    });

function getScrollBarWidth () {
    var fwidth = $(document).width();
    $('html').css('overflow-y', 'hidden');
    var swidth = $(document).width();
    $("html").css("overflow-y", "visible");
    return (swidth - fwidth);
}

var animationsSupported = !$('html').hasClass('lt-ie9') && Modernizr.cssanimations;
$(document).ready(function() {
    /* Bottom fixed scroll to top button. */
    // Fade in / Fade out when the jumbotron is hidden / visible.
    var scrollToTop = $('.scroll-to-top');
    var scrollToTopOffset = function(){ return $('.jumbotron').outerHeight(); };
    var scrollToTopFadeDuration = 400;
    var scrollTopHighlight = false;
    var fadeScrollToTopInterval;
    var fadeScrollToTop = function(){
        if (!fadeScrollToTopInterval)
            fadeScrollToTopInterval = setInterval(fadeScrollToTop, 500);

        var opacity = 1;
        if(scrollToTop.is('.hide'))
            opacity = 0;
        else if((scrollTopHighlight && scrollToTop.is('.highlight')) || scrollToTop.parent().find('.scroll-to-top:hover').length > 0 || scrollToTop.parent().find('.scroll-to-top:focus').length > 0){
            scrollTopHighlight = false;
            opacity = 1;
        }
        else if(scrollToTop.is('.visible')) {
            opacity = 0.2;
            clearInterval(fadeScrollToTopInterval);
            fadeScrollToTopInterval = null;
        }
        if(scrollToTop.css('opacity') != opacity)
            scrollToTop.css('opacity',opacity);
    };
    scrollToTop.on('mouseover', function (e) {
        fadeScrollToTop();
    });

    var currentScrollTop = $(window).scrollTop();
    var scrollDirection;
    $(window).on('scroll', function(e) {
        // Don't track scroll changes during slie-nav is active.
        if($('body').hasClass('slide-active'))
            return;

        if (!fadeScrollToTopInterval)
            fadeScrollToTop();

        var scrollTop = $(window).scrollTop();
        scrollDirection = currentScrollTop < scrollTop ?'down':'up';
        currentScrollTop = scrollTop;

        var isHidden = scrollToTop.hasClass('hide');
        if ($(this).scrollTop() > scrollToTopOffset()){
            if(isHidden){
                scrollToTop.removeClass('hide');
                if(!animationsSupported)
                    scrollToTop.stop().fadeTo(scrollToTopFadeDuration,1);
            }

            scrollToTop.addClass('visible');
            switch (scrollDirection){
                case'down':
                    scrollToTop.removeClass('highlight');
                    break;
                case'up':
                    scrollTopHighlight = true;
                    scrollToTop.addClass('highlight');
                    break;
            }
        }
        else if(!isHidden){
            var toggle = function(){
                scrollToTop.removeClass('visible');
                scrollToTop.removeClass('highlight');
                scrollToTop.addClass('hide');
            };
            if(animationsSupported)
                toggle();
            else
                scrollToTop.fadeOut(scrollToTopFadeDuration, toggle);

        }
    });
    if($(window).scrollTop() > scrollToTopOffset()){
        scrollToTop.removeClass('hide');
        scrollToTop.show();
    }

    scrollToTop.click(function(event) {
        event.preventDefault();
        $('.jumbotron').one('smoothScrolled', function(){
            scrollToTop.find('a').blur();
        });
        smoothScroll.scrollToElement('.jumbotron');
        return false;
    });
});
$('.scroll-to-top').addClass('hide');

/**
 * Created by Cosmo on 15.10.2014.
 */
$(document).ready(function() {
    if (!window.ga)
        return;
// Toggle google analytics
    toggleGAOpt = function () {
        $('#ga-disable').css('visibility', !window.gaEnabled() ? 'hidden' : 'visible');
        $('#ga-disable').css('display', !window.gaEnabled() ? 'none' : 'inline-block');
        $('#ga-enable').css('visibility', window.gaEnabled() ? 'hidden' : 'visible');
        $('#ga-enable').css('display', window.gaEnabled() ? 'none' : 'inline-block');
    };
    $('#ga-disable').on('click', function () {
        gaOptOut();
        toggleGAOpt();
        return false;
    });
    $('#ga-enable').on('click', function () {
        gaOptIn();
        toggleGAOpt();
        return false;
    });
    toggleGAOpt();
});

$(document).ready(function() {
    $.fn.fitText = function( compressor, options ) {

    // Setup options
    var comp = compressor || 1;
    var settings = $.extend({
      'minFontSize' : Number.NEGATIVE_INFINITY,
      'maxFontSize' : Number.POSITIVE_INFINITY
    }, options);

    return this.each(function(){
      // Store the object
      var $this = $(this);
      // Resizer() resizes items based on the object width divided by the compressor * 10
      var resizer = function () {
        $this.css('font-size', Math.max(Math.min($this.width() / (comp*10), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize)));
      };
      // Call once to set.
      resizer();
      // Call on resize. Opera debounces their resize by default.
      $(window).on('throttledresize.fittext orientationchange.fittext', resizer);
    });
  };
          
    $('.jumbotron h1').fitText( 1, {maxFontSize: '63'} );
    $('.jumbotron h2').fitText( 1.5, {maxFontSize: '36'} );
    $('.jumbotron h3').fitText( 1.75, {maxFontSize: '24'} );
    $('.jumbotron h4').fitText( 2, {maxFontSize: '14'} );
    // Hide Jumbotron headline messages for slide in effect.
    $('.jumbotron h1, .jumbotron h2, .jumbotron h3, .jumbotron h4').hide();
  
    // Setup Jumbotron parallax effect.
    $('.jumbotron').parallax( {
        valueCallback: function( yPos ){
        if(!Response.band(992))
            return [ "70%", yPos ];
        },
        parallaxCallback: function(target, yBgPosition, height, scrollTop){
            // Shift also absolute positioned energy-glow-container canvas.
            target.find('*').filter(function() {
                return $(this).css("position") === 'absolute';
            }).each(function(i,o){
                var e = $(o);
                // Adjust container top
                e.css({'top': parseInt(e.attr('offset-top')) + target.offset().top + yBgPosition + 'px'});

                // Shrink canvas to view port size.
                var canvas = e.find('canvas');
                var canvasHeight = canvas[0].height - 12;
                if(height - scrollTop < canvasHeight + 30){
                    var jHeight = height / 2 - scrollTop;
                    canvas.css({'max-height': (canvasHeight+jHeight)/2 + canvasHeight/2 + 'px'});
                }
                else
                    canvas.css({'max-height': canvas[0].height + 'px'});
            });
        }
    });
    
    function drawGlowGradient( id, alphaMin, alphaMax ){
        var canvas = $(id);
        if(!canvas.length)
            return;
        canvas = canvas[0];
        var context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.rect(0, 0, canvas.width, canvas.height);
        var grd = context.createLinearGradient(0, 0, canvas.width, 0);
        grd.addColorStop(0, 'rgba(255,255,255,'+alphaMin+')');
        grd.addColorStop(0.4, 'rgba(255,255,255,'+(alphaMax-0.1)+')');
        grd.addColorStop(0.5, 'rgba(255,255,255,'+alphaMax+')');
        grd.addColorStop(0.6, 'rgba(255,255,255,'+(alphaMax-0.1)+')');
        grd.addColorStop(1, 'rgba(255,255,255,'+alphaMin+')');

        context.fillStyle = grd;
        context.fill();
    }
    drawGlowGradient('#energy-glow', 0.0, 0.8);

    if($('html').hasClass('lt-ie9') || !Modernizr.cssanimations){
        var increment = 0.05;
        var alphaTransition = function(){
            var container = document.getElementById('energy-glow-container');
            var alpha = container.style.opacity ? Number(parseFloat(container.style.opacity).toFixed(2)) : 0;
            alpha += increment;
            if(alpha === 0 || alpha === 1)
                increment *= -1;
            container.style.opacity = alpha;
            container.style.filter = "alpha(opacity="+alpha*100+")"; // IE8
        };
        setInterval(alphaTransition, 40);
    }
    
    var toggleEnergyGlow = function(){
        
        var energyGlow = $('#energy-glow-container');
        var glowContainer = energyGlow.parent();
        var slideNav = $('#slide-nav');
        var energyGlowHtml = energyGlow.outerHTML();
        
        // Turn glow off before slide-nav is opening.
        energyGlow.addClass('opacityTransition');
        slideNav.on('toggle', function(e, slideActive){
            if(!slideActive){
                var energyGlow = $('#energy-glow-container');
                energyGlow.removeClass('opacityTransition');
                energyGlowHtml = energyGlow.outerHTML();
                energyGlow.remove();
            }
        });
        // Turn glow on before slide-nav is closed.
        slideNav.on('toggled', function(e, slideActive){
            if(!slideActive){
                glowContainer.prepend(energyGlowHtml);
                var energyGlow = $('#energy-glow-container');
                drawGlowGradient('#energy-glow', 0.0, 0.8);
                energyGlow.addClass('opacityTransition');
            }
        });
    };
    setTimeout(function(){
        $('.jumbotron').addClass('opacity-in');
        setTimeout(function(){
            toggleEnergyGlow();
            // Slide down and unblur Jumbotron headline messages.
            
            var blurIn = function( selector, blurLevel, delay, duration ){
                var target = $(selector);
                if(target.length){
                    setTimeout( function() {
                        blur( target, blurLevel );
                        target.addClass( 'slideDown' );
                        blur( target, 0, function(){
                            var id = setInterval(function(){
                                if(Math.round(target.css('opacity')) == 1){
                                    target.removeClass( 'slideDown' );
                                    clearInterval(id);
                                }
                            },500);
                        }, duration );
                        target.show();
                    }, delay );
                }
            };
            blurIn('.jumbotron h4, .jumbotron h4', 5, 0, 1500 );
            blurIn('.jumbotron h3, .jumbotron h3', 6, 0, 1500 );
            blurIn('.jumbotron h2, .jumbotron h2', 7, 1250, 1500 );
            blurIn('.jumbotron h1, .jumbotron h1', 8, 1250, 1500 );
        },500);
    });
});

/* Set bot-invisible email link targets. */
$("a[href='mailto:']").one( 'click', function() {
    var that = $(this);
    that.attr(
        'href',
        that.attr('href') +
        that.children('span').attr('prepend') +
        that.text().trim() +
        that.children('span').attr('append'));
} );

$(document).ready(function() {
    // Setup sitemap modal links
    $('a[href^=#].sitemap-link').each( function(e) {
        var a = $(this);
        var href = a.attr('href');
        if(!$('div[id='+href+']'))
            return;
        a.attr('href', null);

        a.on( 'click', function() {
            $(href).modal();
            window.document.location = href;
        });
    });
});
//# sourceMappingURL=main.js.map