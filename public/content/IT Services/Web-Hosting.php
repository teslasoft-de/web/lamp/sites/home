<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.06.2014
 * File: Web-Hosting.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
Benefit from our know-how in web-hosting and its requirements to secure your accounts and your communication under UNIX/Linux. We can host and also build up the server architecture for your needs.
EOT
);
$page->Save();

SetPageHeader( $page, "We make IT life easier with", "Secure Web-Hosting" );

CreateUnderConstruction( $page, 'en' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

AddPageColumn( $row, 'col-lg-12', 'Web-Hosting', 'Web-Hosting', <<<EOT
Today's web hosting is filled with competitors on the market which offer nearly identical tool side, but on the other side you do not really know how secure your passwords are stored or private and business related e-mails are handled on their way through the internet. Until now, only a small amount of internet users are using existing since 15 years old technologies to protect themselves from potentially security leaks and to defend against security attacks.
<hr/>
Five lines of code and a sophisticated certificate management can be enough to make a password attack nearly impossible. Unfortunately, many vendors are afraid of the use of CPU-intensive algorithms, or certificates with large key lengths, because these policies cause on virtual hosts a high cost factor, as they consume a high degree of computing resources which in turn, cannot be provided for other users. 
<hr/>
We provide all currently available technologies to protect your infrastructure and provide recommendations securing particularly critical areas and give advice about the pros and cons of certain methods and the hardware to be used.
EOT
);

SetFooter( $page, 'TESL<font class="aa">Å</font>SOFT', 'Follow us on $' );
