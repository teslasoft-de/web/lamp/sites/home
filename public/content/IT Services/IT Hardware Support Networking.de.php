<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.06.2014
 * File: IT Hardware Support Networking.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
Das ist die Revolution der PC-Branche! Wir entwickeln die nächste Generation des Hardware-Support für das Comeback der PC-Branche!
EOT
);
$page->Save();

SetPageHeader( $page, "Demnächst...", "IT-Hardware-Support-Networking" );

CreateUnderConstruction( $page, 'de' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

AddPageColumn( $row, 'col-lg-12', 'IT Hardware Support Networking', 'Demnächst: IT-Hardware-Support-Networking', <<<EOT
<div class="text-center">
    <br/>
    <span class="open-sans h4">Mit der Entwicklung von Lösungen um alle PC-Marktbeteiligten zu vernetzen, eliminieren wir zeitraubenden Aufgaben und Wartezeiten - von Auswahl der passenden Hardware und Zubehör, über Bereitstellung, bis hin zur Service-Suche - damit Sie sich auf den Einsatz Ihrer Hardware konzentrieren können.</span>
    <hr/>
    <h3>Das ist die nächste Generation! Das ist das Comeback der PC-Branche!</h3>
    Folgen Sie uns und erleben sie etwas Großartiges was Ihr IT-Leben leichtern wird.
    <h4 class="open-sans label-horizontal text-center">
        <small>
            Über Twitter und Google+ werden wir Sie auf dem Laufenden halten.
        </small><br/>
        Folgen Sie uns: 
        <a href="https://twitter.com/Teslasoft_de"><i class="fa fa-tw"></i>@Teslasoft_de</a>
         | 
        <a title="+TeslasoftDe" rel="publisher" href="https://google.com/+TeslasoftDe"><i class="fa fa-gp"></i>+TeslasoftDe</a>
    </h4>
</div>
EOT
);

SetFooter( $page, 'TESL<font class="aa">Å</font>SOFT', 'Folgen Sie uns auf $' );
