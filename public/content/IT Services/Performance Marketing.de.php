<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.06.2014
 * File: Performance Marketing.de.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
Die Erstellung und Durchführung  von erfolgreichen E-Marketing-Kampagnen, ist eines unserer Kernziele. Wir bieten ein effizientes Adressmanagement, detaillierte Performance-Analysen und eine stetig wachsende Auswahl an Zielgruppenmerkmalen.
EOT
);
$page->Save();

SetPageHeader( $page, "We make IT life easier with", "Trusted E-Mails" );

CreateUnderConstruction( $page, 'de' );

$container = GetContentContainer( $page );

$row = GetRow( $container );
$domainName = $page->getMenu()->getSite()->getDomain()->getName();
AddPageColumn( $row, 'col-lg-12', 'Performance Marketing', 'Performance-Marketing', <<<EOT
Um E-Marketing-Kampagnen erfolgreich durchzuführen müssen nicht nur optimale Voraussetzungen im Bereich Technik gegeben sein. Beginnend mit einer E-Mail-Server-Infrastruktur die, unter Beachtung aktueller Technologien, eine Vertrauensstellung unter den Anbietern garantiert. Über Kampagnenüberwachung, Rückläufer-Verarbeitung und Anbieterkontakt um zusätzlich eine ständige Zustellbarkeit sicherzustellen, muss vielmehr das Angebot bereits im Vorfeld auf Zielgruppen abgestimmt, und die Kampagnen nachträglich analysiert werden, um die größtmögliche Performance auch für künftige Kampagnen zu gewährleisten.
<hr/>
Partizipieren Sie an unserem Know-how im Bereich E-Marketing und unserem Wissen über den Markt. Wir bieten darüber hinaus eine hoch skalierte E-Mail-Server-Architektur und anspruchsvolle Performance-Analysewerkzeuge, um die qualitativ hochwertigsten Kampagnen zu gewährleisten. Wir bieten alle wesentlichen Dienstleistungen für ein erfolgreiches Dialogmarketing mit durchschnittlichen Öffnungsraten von bis zu 40%. Profitieren Sie von unserem Computer-Hardware bezogenen Adresspool mit mehr als 200 Tsd. Adressen und mehreren tausend und stetig wachsenden Zielgruppenmerkmalen. Mit unseren Werkzeugen können Sie Ihre Kampagnen ganz einfach erstellen, überwachen und Statistiken analysieren.
<hr/>
Anfragen zu Buchungen und Preisen beantworten wir Ihnen gerne, unter Angabe der Größe Ihres Unternehmens und der Art des Werbemittels. Die Preise richten sich nach der Produktrelevanz und können per E-Mail an postmaster@$domainName oder über das Kontaktformular angefordert werden.
EOT
);

SetFooter( $page, 'TESL<font class="aa">Å</font>SOFT', 'Folgen Sie uns auf $' );
