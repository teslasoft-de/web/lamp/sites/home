<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.06.2014
 * File: IT-Consulting.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
We are your partner to push your ideas into the right direction. From market analysis to software development requirements to distribution strategies. Profit from years of experience in product development.
EOT
);
$page->Save();

SetPageHeader( $page, "We make IT life easier with", "Broad IT Knowledge" );

CreateUnderConstruction( $page, 'en' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

AddPageColumn( $row, 'col-lg-12', 'IT-Consulting', 'IT-Consulting', <<<EOT
Design, consulting and implementation of projects in the areas of Consumer Software, IT infrastructure, IT security and in-house enterprise software are just as much part of our daily tasks, such as first-, second-, and third-level support in emergencies.
<hr/>
Regardless of this we are continually exploring actual trends of IT and even in the future we will expand your portfolio customer- and future-orientated.
EOT
);

SetFooter( $page, 'TESL<font class="aa">Å</font>SOFT', 'Follow us on $' );
