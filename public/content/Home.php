<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 01.06.2014
 * File: Home.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 **/

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( true );
$page->setDescription( "Teslasoft is developing and distributing application und utility software in the IT consumer segment, offering services related to product development, as well as the construction and operation of project infrastructures for companies." );
$page->setKeywords( 'Teslasoft, Application Software, Utility Software, IT-Services, Product Development, Consumer Software, Data Query Systems, IT-Consulting, Performance Marketing, E-Marketing Campaigns, Web-Hosting, IT Hardware Support Networking, Microsoft Windows, Windows Desktop, c#, .net Framework, php, MySQL, html5, javascript, AppStatic, WebStatic, Treiber-Studio, HardCare, TeslaMail' );
$page->Save();

SetPageHeader( $page, "We make IT life easier.", 'TESL<font class="aa">Å</font>SOFT' );
CreateUnderConstruction( $page, 'en' );

$container = GetContentContainer( $page );

$row1 = GetRow( $container, '1' );

AddHomeColumn( $row1, 'col-md-12', 'Consumer Software' );

AddHomeColumn( $row1, 'col-md-4', 'Performance Marketing' );

AddHomeColumn( $row1, 'col-md-4', 'IT-Consulting' );

AddHomeColumn( $row1, 'col-md-4', 'Web-Hosting' );

$row2 = GetRow( $container, '2' );

AddHomeColumn( $row2, 'col-md-4', 'Data Query Systems' );

AddHomeColumn( $row2, 'col-md-8', 'IT Hardware Support Networking' );

SetFooter( $page, 'TESL<font class="aa">Å</font>SOFT', 'Follow us on $' );