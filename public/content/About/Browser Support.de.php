<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.06.2014
 * File: Browser Support.de.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->Save();

SetWBSTitle( $page, 'Unterstützte Browser' );

$body = GetWBSBody( $page );
$body->SetChild(
    'WebbrowserSupport_Content',
    'Content',
    'p',
    'Diese Website verwendet ein responsives Design und kann von jedem mobilen Gerät aus besucht werden.' );

$body->SetChild(
    'WebbrowserSupport_Table',
    'Table',
    'div[@class="panel-heading"]/strong/text()',
    'Unterstützte Webbowser' );

$body->SetChild(
    'WebbrowserSupport_Footer',
    'Close Button',
    'button',
    'Schließen' );