<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 23.06.2014
 * File: Legal Notice.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( true );
$page->setDescription( "Publisher information according to section 5 of the German Telemedia Act (TMG)." );
$page->setRobots( 'noarchive' );
$page->setGooglebot( 'noarchive' );
$page->setSlurp( 'noarchive' );
$page->setMSNBot( 'noarchive' );
$page->setTeoma( 'noarchive' );
$page->Save();

SetPageHeader( $page, "We make IT life easier.", 'TESL<font class="aa">Å</font>SOFT' );

CreateUnderConstruction( $page, 'en' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

SetPageColumContent(
    $row,
    'col-sm-12',
    'Legal Notice', 'Legal Notice',
    \WebStatic\TEMPLATE_PATH . 'About/Legal Notice/legal-notice.phtml' );

SetFooter( $page, 'TESL<font class="aa">Å</font>SOFT', 'Follow us on $' );
?>
