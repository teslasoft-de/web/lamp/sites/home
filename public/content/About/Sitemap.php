<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 22.06.2014
 * File: Sitemap.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
The website structure of <span class="domain-name">teslasoft.de</span>.
EOT
);
$page->Save();

SetPageHeader( $page, "We make IT life easier.", 'TESL<font class="aa">Å</font>SOFT' );

CreateUnderConstruction( $page, 'en' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

AddPageColumn( $row, 'col-sm-12', 'Sitemap', 'Sitemap' );

SetFooter( $page, 'TESL<font class="aa">Å</font>SOFT', 'Follow us on $' );
