<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 11.07.2014
 * File: Privacy Policy.de.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( "Rechliche Hinweise zum Datenschutz und Cookies." );
$page->Save();

SetPageHeader( $page, "We make IT life easier.", 'TESL<font class="aa">Å</font>SOFT' );

CreateUnderConstruction( $page, 'de' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

SetPageColumContent(
    $row,
    'col-sm-12',
    'Privacy Policy', 'Datenschutzerklärung',
    \WebStatic\TEMPLATE_PATH . 'About/Privacy Policy/privacy-policy.phtml' );

SetFooter( $page, 'TESL<font class="aa">Å</font>SOFT', 'Folgen Sie uns auf $' );