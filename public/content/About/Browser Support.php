<?php

/* 
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 *----------------------------------------------------
 * 15.06.2014
 * File: Browser Support.php
 * Encoding: UTF-8
 * Project: Teslasoft 
 **/

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->Save();

SetWBSTitle( $page, 'Browser Support' );

$body = GetWBSBody( $page );
$body->SetChild(
    'WebbrowserSupport_Content',
    'Content',
    'p',
    'This website uses a responsive design and can be visited on any mobile device.' );

$body->SetChild(
    'WebbrowserSupport_Table',
    'Table',
    'div[@class="panel-heading"]/strong/text()',
    'Supported Webbowsers' );

$body->SetChild(
    'WebbrowserSupport_Footer',
    'Close Button',
    'button',
    'Close' );