<?php

/*
 *  This file and its contents are limited to the author only.
 *  See the file "LICENSE" for the full license governing this code.
 *  Differing and additional copyright notices are defined below.
 * ----------------------------------------------------
 * 01.06.2014
 * File: Consumer Software.de.php
 * Encoding: UTF-8
 * Project: Teslasoft.de
 * */

/* @var $page \WebStatic\Core\Page */

$page->setUnderConstruction( false );
$page->setDescription( <<<EOT
Teslasoft entwickelt und vertreibt seit 2004 erfolgreich Anwendungs- und Dienstprogramme im IT-Endanwendersegment. Unser Ziel ist es, die alltäglichen Aufgaben unserer Anwender zu vereinfachen und so deren Leben zu erleichtern. Mit innovativen Technologien und proprietären dynamischen Datenverwaltungslösungen sind wir in der Lage, hoch skalierbare Software-Lösungen mit hoher Leistung und niedrigen Wartungskosten zu entwickeln.
EOT
);
$page->setKeywords('IT, Endanwender-Software, Produktentwicklung, proprietär, dynamische Datenverwaltungslösungen, innovative Technologien, hohe Leistung, niedrige Wartungskosten, Windows-Desktop, Marketing, E-Commerce, Treiber-Studio, Treiber, Update, Aktualisierung, Treiberaktualisierung, Standardgerätefilter, Mainboard, Interne Peripherie, Externe Peripherie, OEM-Systemerkennung, Chiphersteller-Treiber, OEM-Treiber, OEM-Treiberpaket, Chipsatztreiber, Treiber-Kompilation, Betriebssystem-Wechsel, Online-Hardware-Profil, Cloud, Treiberhistorie, E-Mail-Benachrichtigung, Profilbenachrichtigung, Presseveröffentlichungen, Covermount, Covermount-Veröffentlichungen, Covermount-Medien, Computerzeitschriften, COMPUTER BILD, CHIP, CHIP digital, PC go!, PC Magazin, PC Welt, CD Austria, Fachhandel, Direktvertrieb, Download-Charts, Top 100 Download-Charts, Top 10 Download-Charts, Treibersammlung, Weiterentwicklung, Relaunch, Videobewerbungs-Software, CV One, Mobilgerätemarkt, IT Leben');
$page->Save();

SetPageHeader( $page, "We make IT life easier with", "Utility Software" );

CreateUnderConstruction( $page, 'de' );

$container = GetContentContainer( $page );

$row = GetRow( $container );

AddPageColumn(
    $row,
    'col-lg-12',
    'Consumer Software', 'Endanwender-Software' );

SetPageContent( $page, 'Endanwender-Software', \WebStatic\TEMPLATE_PATH . 'Consumer Software/consumer-software.de.phtml');

SetFooter( $page, 'TESL<font class="aa">Å</font>SOFT', 'Folgen Sie uns auf $' );
