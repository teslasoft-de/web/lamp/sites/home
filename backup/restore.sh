#!/bin/bash
if [[ $EUID -ne 0 ]]; then
  echo "You must be a root user" 2>&1
  exit 1
fi

BACKUP_ROOT="/home/teslasoft/web/htdocs/"
BACKUP_DIR="backup/"
TARGET_DIR="public"
USER_NAME="www-site-admin"
DATA_BASE_NAME="www"

if [ -z $1 ]; then
    echo "Please enter the backup name. (without extension [MyBackup].ext)"
    read BACKUP_FILE
else
    BACKUP_FILE=$1
fi

cd $BACKUP_ROOT

BACKUP_FILE_PATH=$BACKUP_DIR$BACKUP_FILE".tar.gz"

if [ ! -e $BACKUP_FILE_PATH ]; then
    echo Backup file \'$BACKUP_ROOT$BACKUP_FILE_PATH\' not found.
    exit 1;
fi

for sqlFile in "./*.sql"; do
    filename=$(basename "$sqlFile")
    extension="${filename##*.}"
    filename="${filename%.*}"    
    filename=${filename/$DATA_BASE_NAME/$TARGET_DIR};
    
    if [[ $BACKUP_FILE == $filename* ]]; then
        DATABASE_FILE=$sqlFile
        break
    fi
done;
if [ -e $DATABASE_FILE ]; then
    echo Removing old backup files
    rm -f $DATABASE_FILE
fi

echo -e "Type 'del' to delete the entire '$TARGET_DIR' directory: "
read DEL
if [[ $DEL != "del" ]]; then
    echo "Backup Restore aborted."
    exit 1;
fi

rm -rf $TARGET_DIR
/bin/tar -xpzf $BACKUP_FILE_PATH
  
echo Please enter the \'$USER_NAME\' MySQL user password:  
mysql -u $USER_NAME -p $DATA_BASE_NAME < $DATABASE_FILE
rm -f $DATABASE_FILE

echo "Backup '$BACKUP_FILE' restored"
