#!/bin/bash
DATE=$(date +%Y-%m-%d)
#NOTE: These two paths must end in a / in order to correctly build up the other paths
BACKUP_ROOT="/home/teslasoft/web/htdocs/"
BACKUP_DIR="backup/"
#NOTE: The backup directory shouldn't end in a /
TARGET_DIR="public"
USER_NAME="www-site-admin"
DATA_BASE_NAME="www"

echo $DATA_BASE_NAME - $BACKUP_ROOT$TARGET_DIR Backup

DATABASE_FILE=$DATA_BASE_NAME"-"$DATE".sql"
BACKUP_FILE_NAME=$BACKUP_DIR$TARGET_DIR"-"$DATE
COUNT=$(find $BACKUP_ROOT$BACKUP_FILE_NAME* -maxdepth 1 -exec echo \; | wc -l)+1
declare -i BACKUP_FILE_COUNT
BACKUP_FILE_COUNT=$COUNT
BACKUP_FILE_NAME=$BACKUP_DIR$TARGET_DIR"-"$DATE
BACKUP_FILE=$BACKUP_DIR$TARGET_DIR"-"$DATE"-"$BACKUP_FILE_COUNT".tar.gz"

#These directories shouldn't end in a / although
#I don't think it will cause any problems if
#they do. There should be a space at the end though
#to ensure the database file gets concatenated correctly.
SOURCE="./"$TARGET_DIR" ./"$DATABASE_FILE

echo Removing old backup files
rm -f $BACKUP_ROOT$BACKUP_FILE
rm -f $BACKUP_ROOT$DATABASE_FILE

echo Creating database backup
echo Please enter the \'$USER_NAME\' MySQL user password:
mysqldump -Q -u $USER_NAME -B $DATA_BASE_NAME -p > $BACKUP_ROOT$DATABASE_FILE

if [ ! -s $BACKUP_ROOT$DATABASE_FILE ]; then
	echo "Backup failed"
    exit 1;
fi 
chgrp -v $USER_NAME $BACKUP_ROOT$DATABASE_FILE

echo Backing up $SOURCE to file $BACKUP_FILE
/bin/tar -C $BACKUP_ROOT -cpazf $BACKUP_ROOT$BACKUP_FILE $SOURCE
chgrp -v $USER_NAME $BACKUP_ROOT$BACKUP_FILE

#echo Cleanup
rm -f $BACKUP_ROOT$DATABASE_FILE

echo Backup $BACKUP_FILE_COUNT \($DATE\) Completed
